package practical10;

import org.junit.Test;

/**
 * CSCU9A3 - Practical 10 <br />
 * <code>Solver.java</code>
 * 
 * <p>Solver sets up our different problems and tries to solve them in different ways.</p>
 * 
 * @author  David Cairns
 * @version 21.11.2013
 * @since   1.0
 */

public class Solver {    
    /**
     * The following is used to keep track of how many times we call the objective function. <br />
     * It is normally proportional to the amount of work involved in finding a solution to the problem.
     */
    private int evaluations = 0;
    
    /**
     * Set the target to null, initially.
     */
    private Pattern target = null;

    /**
     * Set up the Solver object.
     */
    public Solver() {
        target = new Pattern(40, 40, true); // Create a random 40x40 target pattern
    }

    /**
     * A test for the solvePattern method.
     */
    @Test
    public void solvePattern() {
        // View the pattern we are trying to guess
        System.out.println("Target\n" + target);

        // Use the greedy algorithm to find the pattern
        evaluations = 0;
        Greedy grd = new Greedy();
        @SuppressWarnings("unused")
        Pattern greedyPattern = grd.greedyPattern(target, this);
        System.out.println("Greedy Evaluations " + evaluations + "\n");

        // Now try to get the GA to guess the pattern
        // evaluations = 0;
        // GA ga = new GA();
        // Pattern gaPattern = ga.evolvePattern(target, this);
        // System.out.println("GA Evaluations " + evaluations + "\n");
    }

    /**
     * A test for the solveRoute method.
     */
    @Test
    public void solveRoute() {
        TSP tsp = new TSP();
        tsp.loadLocations("locations.txt");
        @SuppressWarnings("unused")
        String output;

        // Use the following array to calculate the score for a particular route. The values in the route array are indexes
        // to the lines in the file with 0 being the first line.
        int route[] = { 9, 1, 6, 8, 5, 3, 0, 4, 7, 2 };
        // This route would be:
        // Lauder -> Galashiels -> Penicuik -> Livingston -> Stirling ->
        // Callander -> Kilmarnock -> Cumbernauld -> Kinross -> St Andrews
        System.out.println("Distance " + (int) tsp.getRouteLength(route));
        System.out.println("Route: " + tsp.toString(route));
        System.out.println();

        // Try to solve routing problem with a greedy approach
        // tsp.resetEvaluations();
        // Greedy grd = new Greedy();
        // int greedyPath[] = grd.greedyRoute(tsp);
        // double greedyDistance = tsp.scoreRoute(greedyPath);
        // output = String.format("Greedy Evaluations: %d\tRoute Distance:%.1f",
        // tsp.getEvaluations(),greedyDistance);
        // System.out.println(output);
        // tsp.printRoute(greedyPath);
        // System.out.println();
        // Now try a GA approach
        // tsp.resetEvaluations();
        // GA ga = new GA();
        // int gaPath[] = ga.evolveRoute(tsp);
        // double gaDistance = tsp.scoreRoute(gaPath);
        // output = String.format("GA Evaluations: %d\tRoute Distance:%.1f",
        // tsp.getEvaluations(),gaDistance);
        // System.out.println(output);
        // tsp.printRoute(gaPath);
    }

    /**
     * We score our solutions via the following methods so that we can keep a count of how often they are called using
     * the 'evaluations' counter. <br />
     * 
     * This gives a reasonable approximation of the amount of work involved.
     * @param proposed  the proposed score.
     * @param target    the target score.
     * @return          The difference.
     */
    public float scoreSolution(Pattern proposed, Pattern target) {
        evaluations++;
        return proposed.difference(target);
    }

    /**
     * Constructor for the above.
     * @param c         the chromosome.
     * @param target    the target.
     * @return          The score.
     */
    public float scoreSolution(Chromosome c, Pattern target) {
        evaluations++;
        return c.score(target);
    }
}
