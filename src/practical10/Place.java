package practical10;

/**
 * CSCU9A3 - Practical 10 <br />
 * <code>Place.java</code>
 * 
 * <p>
 * Place acts as a simple container to hold the name of a place and its position as 'x' and 'y' coordinates.
 * </p>
 *
 * @author  David Cairns
 * @version 23.11.2013
 * @since   1.0
 */

public class Place {
    /**
     * The name associated with a location.
     */
    private String name;
    
    /**
     * The x coordinate.
     */
    private int xc;
    
    /**
     * The y coordinate.
     */
    private int yc;

    /**
     * Creates a new location based on its name 'n' and the position 'x,y'.
     * @param n the string.
     * @param x the x coordinate.
     * @param y the y coordinate.
     */
    public Place(String n, int x, int y) {
        name = n;
        xc = x;
        yc = y;
    }

    // Attribute accessor methods
    
    /**
     * Get the name.
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the x-coordinate.
     * @return The x-coordinate.
     */
    public int getX() {
        return xc;
    }

    /**
     * Get the y-coordinate.
     * @return The y-coordinate.
     */
    public int getY() {
        return yc;
    }

    /**
     * Calculates the distance between two locations using Pythagoras' theorem.
     * @param p the place to measure the distance to.
     * @return  The distance between 'p' and position stored by this object.
     */
    public double distanceTo(Place p) {
        // Get the difference along the x and y axis
        float xdiff = xc - p.xc;
        float ydiff = yc - p.yc;

        // Use Pythagoras theorem to work out the distance  along the diagonal between the two places
        return Math.sqrt((xdiff * xdiff) + (ydiff * ydiff));
    }
}
