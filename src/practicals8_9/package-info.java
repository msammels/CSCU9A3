/**
 * <p><b>Competitive Coding Challenge</b></p>
 * 
 * <p><b>Introduction</b></p>
 * 
 * This practical spans two weeks. It is a chance for you to practice your skills, have a bit of a coding workout, and
 * hopefully have some fun. 
 * 
 * <strong>
 * As such, there are no checkpoints, and the practical does not count towards your overall
 * grade. So, you are welcome to share ideas and work in teams. There are a couple of prizes for the best submissions!
 * </strong>
 * 
 * The practical is largely based on <a href="https://www.hackerrank.com/challenges/frequency-queries">this challenge</a>
 * and there are helpful discussion boards on that site that you’re welcome to read as well.
 * 
 * <p><b>Practical 8: The code challenge</b></p>
 * 
 * For the first part of the practical, we need to implement a data structure and some supporting code to store and process
 * a series of numbers. These numbers comes from a list of queries that you are provided with. Each query is a pair of
 * values, and looks like one of the following:
 * 
 * <ul>
 *     <li>
 *     x: Insert x into the data structure (more than 1 copy of x is allowed)
 *     </li>
 *     <li>
 *     y: Remove y from the data structure
 *     </li>
 *     <li>
 *     z: Check if any of the values in the data structure occurs exactly z times. If yes, output a 1, if no, output a 0
 *     </li>
 * </ul>
 * 
 * The maximum number of queries possible is 50 000 000. x and y are integers in the range -32768 to 32767 inclusive, and
 * z is a positive integer.
 * 
 * These queries are provided as a 2D array of Strings, where queries[i][0] contains the operation, and queries[i][1]
 * contains the data. For example, if you are given the queries: 1 1, 2 2, 3 2, 1 1, 1 2, 1 2, 3 1, 1 1, 2 1, 1 3, 3 2
 * then the results of each operation are:
 * 
 * <p><img src="{@docRoot}/resources/practicals8_9-img01.jpg" alt ="practicals8_9-img01" height="50%" width="50%" /></p>
 * 
 * <p><b>What you need to do</b></p>
 * 
 * In the directory {@code \\Wide\Groups\CSCU9A3\Practicals\Practicals 8 and 9} you should find a file called
 * {@linkplain ExampleForSharing#ExampleForSharing ExampleForSharing.java} and a folder called {@code Data}. Create a new
 * Eclipse project in your home file space and add these to it: the java file under “src”, and the Data folder in the
 * root of the project.
 * 
 * <br /><br />
 * 
 * Your main job here is to improve the method {@link ExampleForSharing#processQueries processQueries()}. There is already
 * some code there that will solve the problem, by adding / removing the values in to an ArrayList, then sequentially
 * searching the list whenever query type 3 is received. This is far from the best implementation for the problem though:
 * it will run very slowly as the number of queries gets bigger. Think about the data structures we’ve covered and the
 * different algorithms that cover them. Can you do any better? You might want to replace some or all of the method
 * {@code processQueries()}, but please leave the method name, arguments, and return type the same.
 * 
 * <br /><br />
 * 
 * In the rest of the class are two methods to load lists of queries from a file (such as those in “Data/input”) and to
 * compare the list of outputs with the expected outputs listed in another file (those in “Data/output”). The
 * {@link ExampleForSharing#main main()} method gives an example of running the existing implementation once,
 * for one of the example lists of queries. You’ll see that each query file in “Data/input” has a matching file in
 * “Data/output” to compare against for correctness. If you try running the existing on one of the larger query files,
 * you’ll see it takes a long time to run. It’s possibly you’ll get an {@link java.lang.OutOfMemoryError OutOfMemoryError}
 * on the larger files. If this happens you can grant java access to more memory. You can see how to do this at the end of
 * this documentation.
 * 
 * The {@link ExampleForSharing#loadFile loadFile()} method makes some assumptions about the query file format. The file
 * should start with a number (how many queries there are in the file) and then has one line per query; a query is a single
 * digit (1,2, or 3) followed by a space, followed by an integer. Some extra error checking could happen in the
 * {@code loadFile()} method to make it more robust. Add some extra checks, keeping in mind that the second part of the
 * challenge is writing some more query files to give the code of your fellow students a workout. If bad formatting is
 * found, the query file should simply be rejected (i.e. an empty query list returned).
 * 
 * <br /><br />
 * 
 * Once you’re happy with your submission, rename your Java class (your network ID – or a nickname – is fine – as long as
 * it’s unique and a valid Java class name) upload it to the “{@code Practical 8 Competitive Coding Code Submission}”
 * folder, under “{@code Files}” in the {@code CSCU9A3 General channel} on MS Teams. Put your name (or the names of
 * everyone involved if you’re working in a team) in a comment at the top of the source file, plus any notes on your
 * implementation that explain what approach you took to making the code run fast. Multiple entries are allowed!
 * 
 * <br /><br />
 * 
 * So, to recap, this week you should:
 * 
 * <ul>
 *     <li>
 *     Implement data structures and code to search them within
 *     {@link ExampleForSharing#processQueries processQueries()}, so the queries can be processed as quickly as possible
 *     </li>
 *     <li>
 *     Add error handing to {@link ExampleForSharing#loadFile loadFile()}
 *     </li>
 * </ul>
 * 
 * Once you’ve uploaded your .java files to Teams, Dr Brownlee will compile and run them on a high- end Xeon workstation.
 * Each {@link ExampleForSharing#processQueries processQueries()} method will be tested against the data
 * sets you were provided with, and some hidden ones. The methods will be run 20 times and the average time to process all
 * the queries will be posted on a leaderboard on Canvas. To avoid the trials taking forever, programs that run for longer
 * than a threshold (to be determined, but probably the running time for the example code) will be deemed to have failed.
 * (the {@code loadFile()} method does not count towards the timings)
 * 
 * <br /><br />
 * 
 * The original plan to provide a box of sweets to the person or team producing the fastest running code is clearly not
 * going to work this semester, so instead we will provide a £10 Amazon voucher to whoever writes the fastest code that
 * produces the correct answers. This will be whoever is top of the Canvas leaderboard at 5pm on Thursday 3 December 2020.
 * 
 * <p><b>Practical 9: Testing / New Queries</b></p>
 * 
 * As a supplement to the code challenge, you are also invited to submit challenging test cases of your own. These tests
 * will then be applied to all the submissions from the class. A second Canvas leaderboard will rank these tests such that
 * tests that cause the most of the submitted programs to fail will be scored highest.
 * 
 * <br /><br />
 * 
 * Your tests should take the form of new Query files in the same format as the original ones provided for the challenge.
 * A couple of ideas for things you can try:
 * 
 * <ul>
 *     <li>Poor formatting of queries: missing spaces etc</li>
 *     <li>Scenarios that are likely to trigger long running time or high memory consumption</li>
 * </ul>
 * 
 * The query files can be called anything you like (respecting taste and decency), with a .txt extension. Upload your
 * files into a folder with your name / student ID within the “{@code Practical 9 Competitive Coding Tests Submission}”
 * folder, under “{@code Files}” in the {@code CSCU9A3 General channel} on MS Teams.
 * 
 * You don’t need to provide expected outputs, though you can if you wish (make sure the filename makes it clear they are
 * outputs). Dr Brownlee will generate outputs using a more robust version of the example code to ensure your tests are
 * actually passable.
 * 
 * <br /><br />
 * 
 * A <b>£5 Amazon voucher</b> will be awarded to whoever writes the most difficult test case. This will be whoever is
 * top of the Canvas leaderboard at 5pm on Thursday 3 December 2020.
 * 
 * <br /><br />
 * 
 * Following the 3 December, Dr Brownlee will share the ideas from the best solutions to both parts of this practical
 * with the class via Canvas.
 * 
 * <p><b>Appendix: Running Java with more RAM</b></p>
 * 
 * The larger data files here will gobble up Java’s default maximum heap size (the amount of memory it’s allowed to use),
 * giving an {@code OutOfMemoryError}. You can increase this limit, assuming your computer has enough RAM to do so
 * (or alternatively, you can just use the smaller files for testing).
 * 
 * <br /><br />
 * 
 * Increase the maximum heap size for Java by adding {@code -Xmx8G} on the command line, or to “VW Arguments” under
 * <b>Run Configurations / Arguments</b> in Eclipse. (8G specifies that Java can have up to 8GB of RAM – bigger or smaller
 * numbers also allowed).
 * 
 * In a console, that would look like this if you’re running a class called {@code MyClass}:
 * 
 * <p><img src="{@docRoot}/resources/practicals8_9-img02.jpg" alt ="practicals8_9-img02" height="25%" width="25%" /></p>
 * 
 * In Eclipse, you can specify this limit in the <b>Run Configuration</b> associated with your class. You access the run
 * configurations by clicking on the dropdown next to the “run” button, and clicking “<b>Run Configurations</b>"
 * 
 * <p><img src="{@docRoot}/resources/practicals8_9-img03.jpg" alt ="practicals8_9-img03" height="25%" width="25%" /></p>
 * 
 * Pick the Run Configuration for the class you’re running. Here, we’re choosing one for a class called “Dijkstra”
 * 
 * <p><img src="{@docRoot}/resources/practicals8_9-img04.jpg" alt ="practicals8_9-img04" height="25%" width="25%" /></p>
 * 
 * Under <b>VM arguments</b>, add the extra memory option as you can see above. Finally, click “run” to run the class. The
 * next time you run it, this option will be remembered.
 */
package practicals8_9;