package practical4;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CSCU9A3 - Practical 4 <br />
 * <code>Module.java</code>
 *
 * @author  Michael Sammels
 * @version 16.10.2020
 * @since   1.0
 */

public class Module {
    /**
     * Used to log data.
     */
    private static Logger logger;
    
    /**
     * A student list.
     */
    private int[] list;

    /**
     * Get a list and start logging it.
     * @param module_name   the name of the module.
     * @param list          the list of students.
     */
    public Module(String module_name, int[] list) {
        this.list = list;

        logger = Logger.getLogger("Logging module " + module_name);
        logger.setLevel(Level.SEVERE);  // This will only log/print SEVERE type messages
    }

    /**
     * Gets a list of modules.
     * @return  The list of modules.
     */
    public int[] getList() {
        return list;
    }

    /**
     * Add a student.
     * @param s the student number to add.
     */
    public void add_student(int s) {
        logger.log(Level.INFO, "Adding student {0}", s);
        if (find_sequential(s) == -1) { // Check if the list contains student s
            logger.log(Level.INFO, "Current student list {0}", Arrays.toString(list));

            int[] new_list = new int[list.length + 1];
            System.arraycopy(list, 0, new_list, 0, list.length);

            new_list[list.length] = s;
            list = new_list;

            logger.log(Level.INFO, "New current student list {0}", Arrays.toString(list));
        } else {
            logger.log(Level.WARNING, "You can not add the same student twice");
        }
    }

    /**
     * Remove a student.
     * @param s the student number.
     */
    public void remove_student(int s) {
        logger.log(Level.INFO, "Removing student {0}", s);
        logger.log(Level.INFO, "Current student list {0}", Arrays.toString(list));

        try {
            int[] new_list = new int[list.length - 1];
            for (int i = 0, c = 0; i < list.length; i++) {
                if (list[i] != s) {
                    new_list[c++] = list[i];
                }
            }
            list = new_list;
            logger.log(Level.INFO, "New current student list {0}", Arrays.toString(list));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Could not remove student. Error: {0}", e);
        }
    }

    /**
     * The Bubble Sort algorithm.
     */
    public void sort() {
        // This is the bubble sort, the simplest sorting algorithm
        for (int i = 0; i < (list.length - 1); i++) {
            for (int j = 0; j < list.length - i - 1; j++) {
                if (list[j] > list[j + 1]) {
                    int temp = list[j];
                    list[j] = list[j + 1];
                    list[j + 1] = temp;
                }
            }
        }
    }

    /**
     * Sequential searching.
     * @param s the student number to search for.
     * @return  The found results.
     */
    public int find_sequential(int s) {
        int index = -1;
        for (int i = 0; i < list.length; i++) {
            if (list[i] == s) {
                index = i;
            }
        }
        return index;
    }

    /**
     * Binary searching.
     * @param element   the element to search for.
     */
    public void find_binary(int element) {
        int lowIndex = 0;
        int highIndex = list.length - 1;

        // Holds the element position in array for given element. Initial negative integer set to be returned if no
        // match was found in array

        // If lowIndex less than highIndex, there's still elements in the array
        while (lowIndex <= highIndex) {
            int midIndex = (lowIndex + highIndex) / 2;
            if (element == list[midIndex]) {
                break;
            } else if (element < list[midIndex]) {
                highIndex = midIndex - 1;
            } else if (element > list[midIndex]) {
                lowIndex = midIndex + 1;
            }
        }
    }
}
