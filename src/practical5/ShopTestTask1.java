package practical5;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.Queue;
import org.junit.Before;
import org.junit.Test;

/**
 * CSCU9A3 - Practical 5 <br />
 * <code>ShopTestTask1.java</code>
 *
 * @author  Michael Sammels
 * @version 23.10.2020
 * @since   1.0
 */

public class ShopTestTask1 {
    /**
     * A new Shop object.
     */
    private Shop shop;

    /**
     * Instantiate the object before the test.
     */
    @Before
    public void setUp() {
        shop = new Shop();
    }

    /**
     * Test the initial state.
     */
    @Test
    public void testInitialState() {
        Queue<Customer> queue = shop.getQueue();
        assertTrue("Shop queue not empty", queue.isEmpty());
    }

    /**
     * Test adding a customer.
     */
    @Test
    public void testAddCustomer() {
        Customer customer = new Customer();
        shop.addCustomer(customer);

        Queue<Customer> queue = shop.getQueue();
        assertEquals("Customer not added successfully to the queue.", 1, queue.size());
        assertEquals("Customer not added successfully to the queue.", customer, queue.peek());
    }

    /**
     * Test getting the next customer.
     */
    @Test
    public void testGetNextCustomer() {
        Customer customer = new Customer();
        shop.addCustomer(customer);

        assertEquals("Customer not added successfully to the queue.", 1, shop.getQueue().size());
        assertEquals("Customer not added successfully to the queue.", customer, shop.getNextCustomer());
    }
}
