/**
 * <p><b>Queues and more queues!!!</b></p>
 * 
 * <p><b>Intro</b></p>
 * 
 * This practical will introduce you to the Java documentation and some BlueJ/Eclipse shortcuts. You will then build a
 * shop with various queues, continuously running the unit tests to ensure that your code is correct.
 * 
 * <p><b>Javadocs</b></p>
 * 
 * Documentation is boring to write, but very handy if you’re a programmer needing to find out what code does. Let’s
 * make use of the Javadocs, documentation to describe Java classes, to learn what methods are available to us for the
 * ADTs; Stacks and Queues.
 * 
 * Check the online documentation for Java
 * <a href="https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Stack.html">Stack</a> and <br />
 * <a href="https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Queue.html">Queues</a>
 * 
 * 
 * Have a look around the page to get an idea for what is going on. Half way down you will see a ‘Method Summary’ list,
 * showing the methods that we would expect for a Stack (peek, pop and push) and Queue (add, peek, poll).
 * 
 * <p><b>Task</b></p>
 * 
 * Find out the method names used in Java for dequeueing and enqueueing an item from a queue.
 * 
 * <p><b>BlueJ/Eclipse Shortcuts</b></p>
 * 
 * Here are some shortcuts to make your life a little easier (if you don’t already know them).
 * 
 * <p><b>Compiling</b></p>
 * 
 * To compile in BlueJ (from any window) press CTRL+K. <br />
 * To compile in Eclipse press CTRL+F11
 * 
 * <p><b>Magic Fix Layout</b></p>
 * 
 * This is a wonderful feature of BlueJ (only because it is sorely required). When you have a class open, press
 * CTRL+Shift+I and watch your code tidy itself. (Now you have no excuse for showing demonstrators/submitting
 * assignments with poorly laid out code!).
 * 
 * <br /><br />
 * 
 * The same outcome can be achieved in Eclipse by pressing CTRL+Shift+F.
 * 
 * <p><b>Viewing Documentation</b></p>
 * 
 * In BlueJ, when you have a class open there is a pull-down menu at the top right of the screen, you can toggle it
 * between source code and documentation. For classes which have good documentation this can be a handy way to view this
 * documentation.
 * 
 * <br /><br />
 * 
 * In Eclipse, when you hover your mouse over the Class import and press F2, the documentation is presented.
 * 
 * 
 * <p><b>Method Name Auto-Completion</b></p>
 * 
 * For Eclipse and BlueJ, at any stage when you are coding you can access a list of available methods to you.
 * Hit CTRL+space to see them, navigate using the cursor keys and select using enter.
 * 
 * <br /><br />
 * 
 * If you want to access a method within another class you need to access it via an object. This would be
 * {@code objectName.methodName(parameters)}, for example {@code myInstanceVariable.methodInsideClass()}. This shortcut
 * works well in this situation, as you can type “{@code myInstanceVariable.}” then hit CTRL+space to get a list of all
 * the available (public) methods within that class.
 * 
 * <p><b>The Problem: Coffee Shop</b></p>
 * 
 * We have a coffee shop, which has a single queue of customers. We can add a customer to the queue, get the next customer
 * in the queue, and get the entire queue.
 * 
 * <p><img src="{@docRoot}/resources/practical5-img01.jpg" alt ="practical5-img01" height="25%" width="25%" /></p>
 * 
 * <p><b>Note</b></p>
 * 
 * You will notice that the methods have all been made public, and you’d be right to question this. The methods are public
 * to allow the unit tests to access them.
 * 
 * <p><b>Task 1 – Warm Up</b></p>
 * 
 * Copy the code for this practical from: {@code Groups On Wide > CSCU9A3 > Practicals > Practical 5}
 * 
 * <br /><br />
 * 
 * Remember that if you are accessing Groups On Wide from outside the university, you will need to be connected to the
 * VPN. More information on how to configure/use the VPN can be found here:
 * 
 * <a href="https://stir.unidesk.ac.uk/tas/public/ssp/content/detail/knowledgeitem?unid=f2963fcee9b644c883e0aa443d10bb43">here</a>.
 * 
 * <br /><br />
 * 
 * Open the code for this practical (note, {@link ShopTestTask2#ShopTestTask2 2},
 * {@link ShopTestTask3#ShopTestTask3 3} and {@link ShopTestTask4#ShopTestTask4 4} will not compile – this is OK) and fill
 * in the method bodies {@link Shop#addCustomer addCustomer} and {@link Shop#getNextCustomer getNextCustomer}. Run the
 * {@link ShopTestTask1#ShopTestTask1 ShopTestTask1} tests to ensure that you have correct code.
 * 
 * <p><b>Task 2 – Multiple Tills</b></p>
 * 
 * Our lovely boss in the coffee shop bought some new tills! Instead of having one queue, we now have multiple queues,
 * one for each open till. The number of tills available in our shop is passed in as a parameter to our constructor.
 * Observe the changes made to our shop class:
 * 
 * <p><img src="{@docRoot}/resources/practical5-img02.jpg" alt ="practical5-img02" height="25%" width="25%" /></p>
 * 
 * Make the necessary changes to your Shop class to reflect the addition of new tills. (Note: tillNumber starts at 1). Run
 * the {@link ShopTestTask2#ShopTestTask2 ShopTestTask2} tests to ensure that you have correct code.
 * 
 * <p><i>Hint</i></p>
 * 
 * To initialise the array of Queues you will need the following code:
 * 
 * <pre>new Queue[numberOfTills]</pre>
 * 
 * Instead of one queue, we now have an array of queues. For each queue in the array you will need to initialise it.
 * (Look at the code from Task 1 to see how to declare and initialise one queue).
 * 
 * <p><b>Task 3 – Check Point: Shortest Queue</b></p>
 * 
 * Instead of adding a customer to a specific queue, let’s add the customer to the shortest queue. Add a new method
 * to the {@link Shop#Shop Shop} class called {@link Shop#getShortestQueue getShortestQueue} that returns a reference
 * to the shortest queue in the array of queues. This new method will be used by
 * {@link ShopTestTask3#ShopTestTask3 ShopTestTask3} to check that we can now add a customer to the shortest queue. When
 * you think you have it working, run the {@link ShopTestTask3#ShopTestTask3 ShopTestTask3} tests to ensure that your
 * code is correct.
 * 
 * <p><i>Hints</i></p>
 * 
 * Avoid looking this up – there is no magical solution. Consider the steps you take when choosing a till at a
 * supermarket. Next, put this on paper, and finally pseudocode. Only at this stage when you are comfortable with the
 * logic move should you move onto code.
 * 
 * <p><b>Task 4 – New System</b></p>
 * 
 * That boss of ours decided to stop throwing money around on new tills, and instead he got smart! He noticed that it was
 * all the fancy-coffee buyers that were slowing things down for the simple-coffee buyers, and this was unfair. So
 * instead he decided that his shop would have only two tills, the first till was for simple coffee customers, and the
 * second till for the fancy coffee customers. This allowed the simple coffee buyers a nice speedy way of buying their
 * coffee.
 * 
 * <br /><br />
 * 
 * Make the changes to the {@link Customer#Customer Customer} class to store what kind of coffee buyer they are.
 * 
 * <p><img src="{@docRoot}/resources/practical5-img03.jpg" alt ="practical5-img03" height="25%" width="25%" /></p>
 * 
 * 
 * Make the necessary changes to your {@link Shop#Shop Shop} class to reflect our boss’s new idea (note that you will need a
 * new default constructor for {@link Shop#Shop Shop} that initialises the shop with <b>2 tills</b> – this is same as having
 * <pre>numberOfTills = 2</pre>, in task 2). Run the {@link ShopTestTask4#ShopTestTask4 ShopTestTask4} tests to ensure that
 * your code is correct.
 */
package practical5;
