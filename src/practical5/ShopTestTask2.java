package practical5;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * CSCU9A3 - Practical 5 <br />
 * <code>ShopTestTask2.java</code>
 *
 * @author  Michael Sammels
 * @version 23.10.2020
 * @since   1.0
 */

public class ShopTestTask2 {
    /**
     * The test for the initial state.
     */
    @Test
    public void testInitialState() {
        Shop shop = new Shop(20);

        for (int tillNumber = 1; tillNumber <= 20; tillNumber++) {
            assertNotNull("Queue " + tillNumber + " not initialised within the array.", shop.getQueue(tillNumber));
            assertTrue("Queue " + tillNumber + " not empty", shop.getQueue(tillNumber).isEmpty());
        }
    }

    /**
     * The test for the till number.
     */
    @Test
    public void testTillNumber() {
        Shop shop = new Shop(1);

        try {
            assertNotNull(shop.getQueue(1));
        } catch (ArrayIndexOutOfBoundsException e) {
            fail("Till numbers start at 1, array indexes start at 0!");
        }
    }

    /**
     * The test for adding a customer.
     */
    @Test
    public void testAddCustomer() {
        Shop shop = new Shop(20);

        Customer customer = new Customer();
        shop.addCustomer(customer, 1);

        assertEquals("Customers not added successfully to the queues.", customer, shop.getQueue(1).peek());

        for (int tillNumber = 2; tillNumber <= 20; tillNumber++) {
            assertTrue("Queue " + tillNumber + " should still be empty, we only added to queue 1!", 
                    shop.getQueue(tillNumber).isEmpty());
        }
    }

    /**
     * The test for getting the next customer.
     */
    @Test
    public void testGetNextCustomer() {
        Shop shop = new Shop(20);

        Customer customer1 = new Customer();
        Customer customer2 = new Customer();
        Customer customer3 = new Customer();

        shop.addCustomer(customer1, 1);
        shop.addCustomer(customer2, 3);
        shop.addCustomer(customer3, 1);

        assertEquals("Getting next customer from the queue failed.", customer1, shop.getNextCustomer(1));
        assertEquals("Getting next customer from the queue failed.", customer2, shop.getNextCustomer(3));
        assertEquals("Getting next customer from the queue failed.", customer3, shop.getNextCustomer(1));

        for (int tillNumber = 1; tillNumber <= 20; tillNumber++) {
            assertTrue("Queue " + tillNumber + " is not empty after getting all customers.", 
                    shop.getQueue(tillNumber).isEmpty());
        }
    }
}
