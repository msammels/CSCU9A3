package practical5;

/**
 * CSCU9A3 - Practical 5 <br />
 * <code>Customer.java</code>
 *
 * <p>Representation of a shop customer.</p>
 *
 * @author  Michael Sammels
 * @version 23.10.2020
 */

public class Customer {
    // For this we don't really care what attributes a customer might have, hence the customer class is empty.
    // In a "real" system this wouldn't be the case...

    /**
     * A boolean to determine whether or not the customer is buying a fancy coffee or not.
     */
    public boolean buyingFancyCoffee;

    /**
     * For this we don't really care what attributes a customer might have, hence the customer class is empty.
     * In a "real" system, this wouldn't be the case.
     */
    public Customer() {}

    /**
     * Assign the variable to the boolean.
     * @param buyingFancyCoffee determines whether or not the customer is buying a fancy coffee.
     */
    public Customer(boolean buyingFancyCoffee) {
        this.buyingFancyCoffee = buyingFancyCoffee;
    }
}
