/**
 * <p><b>Colin's Cycle Calculator: Part 1</b></p>
 * 
 * <p><b>Intro</b></p>
 * 
 * In this practical class, we are going to learn how to import a project into Eclipse IDE and recap some Java concepts.
 * 
 * <p><b>The Problem</b></p>
 * 
 * Colin owns a small bicycle shop in Stirling, and has worked there for over 20 years. Over the years he has grown tired
 * of the countless number of people who have asked him how long their particular cycle ride would take. He has written
 * down a set of secret rules that he uses to estimate an answer and has come to you to turn his rules into a computer
 * algorithm, as he’d like to offer this as a service on his shop website. Colin’s rules are:
 * 
 * <ol>
 *     <li>
 *     Firstly, gauge the cyclist’s competency to work out their base average speed.
 *     <ul>
 *         <li>Beginner: 10mph</li>
 *         <li>Intermediate: 15mph</li>
 *         <li>Advanced: 20mph</li>
 *     </ul>
 *     </li>
 *     <li>
 *     Cycling alone or with someone else?
 *     <ul>
 *         <li>Cycling together increases the cyclist’s base average speed by 20%.</li>
 *     </ul>
 *     </li>
 *     <li>
 *     The number of years of experience is crucial to the average speed, as cyclists build up appropriate muscles over the
 *     years.
 *     <ul>
 *         <li>Increase the average speed by 0.2mph for every year experience. 4.</li>
 *     </ul>
 *     </li>
 *     <li>
 *     Finally, the weather has an impact on the average speed during the cycle.
 *     <ul>
 *         <li>For every degree Celsius lower than 10, decrease the average speed by 0.1mph.</li>
 *         <li>For every degree Celsius over 20, decrease the average speed by 0.1mph.</li>
 *         <li>For every set of 15mph winds, decrease the average speed by 1mph.</li>
 *         <li>If it is raining, decrease the average speed by 2mph.</li>
 *     </ul>
 *     </li>
 * </ol>
 * 
 * Knowing Colin’s top-secret information, it is over to you to devise a way to calculate the estimated number of hours
 * that a particular cycle would take.
 * 
 * <p><b>Getting Started</b></p>
 * 
 * A skeleton code, related to this practical, has been created for you. These can be found in
 * 
 * <br /><br />
 * 
 * {@code Groups On Wide > CSCU9A3 > Practicals > Practical 1}
 * 
 * <br /><br />
 * 
 * If you are accessing {@code Groups On Wide} from outside the university, you will need to be connected to the VPN. More
 * information on how to configure/use the VPN can be found
 * 
 * <a href="https://stir.unidesk.ac.uk/tas/public/ssp/content/detail/knowledgeitem?unid=f2963fcee9b644c883e0aa443d10bb43">here</a>
 * 
 * Copy these files to a new project directory and open them in your desired IDE. If you want to use Eclipse (we strongly
 * recommend that you use it), check the section below to learn how to import projects into your workspace. Here, we are
 * using Eclipse Version 2019-06 (4.12.0). You may use other version of Eclipse, however note that some configurations may
 * change slightly.
 * 
 * <p><b>Installation</b></p>
 * 
 * If you have already downloaded and installed Java and Eclipse, please go to the next section. Otherwise, we are using
 * Java 11 SDK, that can be downloaded <a href="https://www.oracle.com/java/technologies/javase-jdk11-downloads.html">here</a>.
 * You'll need to register for a (free) Oracle account to make the download, if you don't have one already.
 * 
 * To download Eclipse, use <a href="https://www.eclipse.org/downloads/">this link</a>. When you run the installer,
 * select "Eclipse IDE for Java Developers".
 * 
 * <p><b>Eclipse</b></p>
 * 
 * Let’s import your code so we can start. When you first open the Eclipse IDE, it will ask you about your workspace, which
 * is a directory that will be used to store all your codes produced in Eclipse. You may choose any directory you want,
 * but in your case, a good practice would be to create a folder for each course. For example, you can create a
 * folder C:\Users\YourName\CSCU9A3\ to store all code produced in our course. <strong>Remember this folder, because every
 * time you open Eclipse, you will need to inform it</strong>.
 * 
 * <p><img src="{@docRoot}/resources/practical1-img01.jpg" alt ="practical1-img01" height="50%" width="50%" /></p>
 * 
 * When you select the directory and launch the IDE, you may see something like this:
 * 
 * <p><img src="{@docRoot}/resources/practical1-img02.jpg" alt ="practical1-img02" height="50%" width="50%" /></p>
 * 
 * This is just a welcome window. You can close it and finally access the IDE, which should have no projects and will
 * probably look like this:
 * 
 * <p><img src="{@docRoot}/resources/practical1-img03.jpg" alt ="practical1-img03" height="50%" width="50%" /></p>
 * 
 * You can create or import JAVA projects now. In this case, we will create a new project. So, click on “Create a Java
 * project”, named it practical-1, and click on “Finish”:
 * 
 * <p><img src="{@docRoot}/resources/practical1-img04.jpg" alt ="practical1-img04" height="50%" width="50%" /></p>
 * 
 * Sometimes, you may see a window asking for the Module Name. In this case, you can just click on “Don’t Create”.
 * 
 * <br /><br />
 * 
 * Your project should be created by now and your workspace should look like this:
 * 
 * <p><img src="{@docRoot}/resources/practical1-img05.jpg" alt ="practical1-img05" height="50%" width="50%" /></p>
 * 
 * Let’s now import the Java codes of this practical to our project. Right click over the src directory and then click on
 * “Import...”. On the import window, select “File System”:
 * 
 * <p><img src="{@docRoot}/resources/practical1-img06.jpg" alt ="practical1-img06" height="50%" width="50%" /></p>
 * 
 * Browse to the aforementioned skeleton directory and select the classes to import:
 * 
 * <p><img src="{@docRoot}/resources/practical1-img07.jpg" alt ="practical1-img07" height="50%" width="50%" /></p>
 * 
 * Now, your workspace should be like this:
 * 
 * <p><img src="{@docRoot}/resources/practical1-img08.jpg" alt ="practical1-img08" height="50%" width="50%" /></p>
 * 
 * Now that we have set our workspace, we can start working on the practical.
 * 
 * <p><b>First Version</b></p>
 * 
 * This code has two classes: {@link Main#Main Main} and {@link CycleCalculator#CycleCalculator CycleCalculator}. The
 * former has the {@link Main#main main} method and can be used to “test” your code. Each print within this main method
 * is related to a “test”. If the print outputs {@code false}, then your code is not working properly. If it displays
 * {@code true}, then your code is fine. In the next week, we’ll see a better way of doing this test process. The later has
 * only method headings and you are responsible for implementing them. The first one you need to implement is the method
 * with the following heading:
 * 
 * <pre>
 * public double getDuration(int numMiles, String competency, int numYearsExperience, boolean cyclingAlone,
 *                           int temp, int windSpeed, boolean isRaining)
 * </pre>
 * 
 * Fill in the logic within this method and return Colin’s estimated number of hours that the cycle will take.
 * 
 * <br /><br />
 * 
 * <b>Notes</b>
 * <ul>
 *     <li>Duration (hours) = distance (miles) / speed (mph)</li>
 *     <li>To increase x by 20%: x = x*1.2</li>
 * </ul>
 * 
 * Do not be tempted to jump straight onto the computer and start coding. Use the techniques taught to you in lectures to
 * break the problem down on paper and produce pseudo-code to solve the problems. Only then, once you are happy with your
 * solution on paper, should you move onto coding the cycle calculator.
 * 
 * <br /><br />
 * 
 * <b>Constants</b><br />
 * Can you imagine if Colin decided to tweak some of his values in his rules? It would require you, as the programmer, to
 * dive into the code and work out which numbers to change. This isn’t particularly nice is it? When programming you can
 * have <b>constants</b>, which are properties whose values do not change. These are typically declared, and values
 * assigned, at the top of a class. In Java we would write:
 * 
 * <pre>
 * private final int BEGINNER_SPEED = 10;
 * </pre>
 * 
 * {@code final} is used to denote that the property value will never change, showing that the property is a constant.
 * You’ll notice that the property name was written in uppercase; this is common practice in many programming languages
 * where constants are written in full uppercase with words separated by an underscore.
 * 
 * <br /><br />
 * 
 * <b>Go through your code</b> and refactor any constants that exist and appreciate exactly why this makes your code a
 * lot better (constants for low and high temperature thresholds are quite useful).
 * 
 * <p><b>Calculating total time: arrays</b></p>
 * 
 * If a person wants to calculate the total time of several bike rides he has made in the last years, he will not be able
 * to do so using your code. So now you will program your code to perform this type of calculation.
 * 
 * <br /><br />
 * 
 * The ride times will be store in arrays. A method has been provided for you to fill in the above method, which receives
 * an array of durations (as {@code double} values) and must return the total of all these durations added together (as a
 * {@code double}):
 * 
 * <pre>
 * public double getTotalDuration(double[] durations)
 * </pre>
 * 
 * In order to check this method, uncomment the last lines of the {@link Main#main main} method. These lines are responsible
 * for checking the {@link CycleCalculator#getTotalDuration getTotalDuration} method.
 * 
 * <p><b>Time Format</b></p>
 * 
 * Calling our {@link CycleCalculator#getDuration getDuration} method returns the time in  a horrible format. For example
 * 1.75 hours would be far nicer written as “1 hour and 45 mins”. Create an appropriate method within CycleCalculator
 * class and get it to return a nicely formatted time as a String. The method signature would look something like:
 * 
 * <pre>
 * public String getFormattedDuration(double time)
 * </pre>
 * 
 * There is <b>no</b> “test” provided for this, so add 3 additional tests into the {@link Main#main main} in order to
 * “test” this method. You should aim to try cases where the duration is exactly on an hour boundary (1.99, 2.01, 2.00, ...)
 * and ones where it is not. Hint: You may find the Math.floor method and the modulus operator (%)
 * useful here.
 * 
 * <p><b>Java Reminders</b></p>
 * 
 * <b>Variables</b>
 * 
 * <pre>
 * int myInteger;
 * int myInitialisedInteger = 25;
 * double bigFloatingPointNumVar = 6.21;
 * String niceString = “Awesome”;
 * </pre>
 * 
 * <b>Conditionals</b>
 * 
 * <pre>
 * if ({@literal <condition>}) {
 *     // Do things!
 * }
 * </pre>
 * 
 * <b>{@literal <condition>} examples:</b>
 * 
 * <ul>
 *     <li><pre>speed {@literal <} 20</pre></li>
 *     <li><pre>cyclingAlone == false (or !cyclingAlone)</pre></li>
 *     <li><pre>isRaining == true (or isRaining)</pre></li>
 *     <li><pre>competence.equals(“Beginner”) (notice for string comparisons we use ‘.equals’ instead of ‘==’)</pre></li>
 * </ul>
 * 
 * 
 * <p><b>Loops</b></p>
 * 
 * <b>For</b>
 * 
 * <pre>
 * for (initialise; condition; step) {
 *     // Do stuff
 * }
 * </pre>
 * 
 * <b>Example: a loop that will run 10 times</b>
 * 
 * <pre>
 * for (int index = 0; index {@literal <} 10; index++) {
 *     // Do stuff
 * }
 * </pre>
 * 
 * <b>While</b>
 * 
 * <pre>
 * Initialise;
 * while (condition) {
 *     // Do stuff
 *     Increment;
 * }
 * </pre>
 * 
 * <b>Example: a loop that will run 10 times</b>
 * 
 * <pre>
 * int index = 0;
 * 
 * while (index {@literal <} 10) {
 *     // Do stuff
 *     index++;
 * }
 * </pre>
 */
package practical1;