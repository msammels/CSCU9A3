/**
 * <p><b>Trees, Hashtables {@literal &} APIs</b></p>
 * 
 * <p><b>Part 1: Trees</b></p>
 * 
 * This practical is aimed at giving you some practice with building and manipulating trees and experimenting with
 * hashtables. This will probably take you longer than two hours, but next week’s practical is a bit shorter so there is
 * no problem in letting this one spill over to then.
 * 
 * <br /><br />
 * 
 * For the first part of the practical, you will start with a simple binary tree node that holds an integer value and a
 * method to add nodes to the tree.
 * 
 * <br /><br />
 * 
 * Copy the directory {@code \\Wide\Groups\CSCU9A3\Practicals\Practical6} to a suitable location in your own file space.
 * Create an Eclipse based project that contains the Java files in the copied directory. You should find classes called
 * {@link IntTreeNode#IntTreeNode IntTreeNode} and {@link IntegerTree#IntegerTree IntegerTree} and a JUnit test called
 * {@link IntTreeTest#IntTreeTest IntTreeTest}. We’ll come back to the {@link BankAccount#BankAccount BankAccount} and
 * {@code Experiment} classes shortly. {@link IntTreeNode#IntTreeNode IntTreeNode} represents a binary node consisting of
 * an integer value and references to left and right child nodes. {@link IntegerTree#IntegerTree IntegerTree} provides
 * methods to add nodes to a tree built from {@link IntTreeNode#IntTreeNode IntTreeNode} and to perform an in-order
 * traversal of the tree and also a method to print out the tree (viewed from the side – the root is on the left). For the
 * tree built by {@link IntTreeTest#IntTreeTest IntTreeTest}, it will print the lines on the left below; right-way up the
 * tree looks like the one on the right.
 * 
 * <p>
 * <img src="{@docRoot}/resources/practical6-img01.jpg" alt ="practical6-img01" height="10%" width="10%" />
 * <img src="{@docRoot}/resources/practical6-img02.jpg" alt ="practical6-img02" height="20%" width="20%" />
 * </p>
 * 
 * The results of the in-order traversal are returned as a {@link java.lang.String String} that you can print out and use
 * to check that you are getting the correct results. {@link IntTreeTest#IntTreeTest IntTreeTest} performs some simple
 * tests to confirm that the above methods work as intended.
 * 
 * <br /><br />
 * 
 * Read through the code and ensure that you understand how it works then try adding some new nodes in the method
 * {@code IntTreeTest.inorderTest} and checking the results with new tests.
 * 
 * <p><b>Adding new traversal methods</b></p>
 * 
 * You should notice that {@link IntegerTree#IntegerTree IntegerTree} contains empty methods for walking the trees. Using
 * the  {@link IntegerTree#printTree printTree} method as a guide, implement the methods
 * {@link IntegerTree#preOrder IntegerTree.preOrder}, {@link IntegerTree#inOrder IntegerTree.inOrder} and
 * {@link IntegerTree#postOrder IntegerTree.postOrder} and then write suitable tests in
 * {@link IntTreeTest#testWalks IntTreeTest.testWalks} to confirm that they work. Note that instead of printing out a
 * node, all you have to do to ‘visit’ it is to append the nodes contents to a StringBuffer as follows:
 * 
 * <pre>sb.append(n + ",");</pre>
 * 
 * Try altering the order in which the initial set of nodes are added to the tree in
 * {@link IntTreeTest#testWalks testWalks} and confirm that the results of walking the trees with a pre-order,
 * in-order and post-order walk are what you expect. Note how the initial node added to the tree can skew the tree that
 * is produced.
 * 
 * <p><b>Part Two: Hashtables {@literal &} APIs</b></p>
 * 
 * In the rest of this practical we’ll be looking at hashtables, but in the real world you’re unlikely to be implementing
 * these kinds of structures yourself. Instead, we’ll be using the ready-made implementations that come included with Java
 * in the Collections Framework. You can see the documentation for this in the
 * <a href="https://docs.oracle.com/javase/8/docs/api/?java/util/Collections.html">Java API</a>. There are several
 * other third party libraries that implement their own collections (Google Guava and Apache Commons being
 * well-known examples) but we’ll stick with the Java ones just now. We’ll also take a look at how to use Java’s
 * ready-made methods for sorting arrays.
 * 
 * <br /><br />
 * 
 * You’ll learn a few things. Firstly, designing your own code to make use of an existing API, and the importance of
 * meeting the API’s contract. We’ll run some experiments to see the performance difference that arises from taking
 * shortcuts. We’ll also have a brief introduction to “random” numbers. I’ve highlighted a few useful new concepts that
 * are also worth remembering as we go; you’ve probably not seen these before but if you have you have a bit of a head
 * start already!
 * 
 * <br /><br />
 * 
 * <strong>
 * This part is challenging, and you should expect to take some time to complete it. However, the tasks of implementing
 * code to match an API are exactly the kind of thing you might encounter in a real programming job, so take your time
 * and enjoy!
 * </strong>
 * 
 * 
 * <p><b>Setup and Template Code</b></p>
 * 
 * In the template classes {@link BankAccount#BankAccount BankAccount}, {@link Experiment1#Experiment1 Experiment1},
 * {@link Experiment2#Experiment2 Experiment2} and {@link Experiment3#Experiment3 Experiment3}, there
 * is some commented-out code to save you lots of tedious copy-and-pasting. Leave this alone until you are told to
 * uncomment it.
 * 
 * <br /><br />
 * 
 * First up, we have an example {@link BankAccount#BankAccount BankAccount} class to represent bank accounts. You’ll see
 * that it’s pretty simple. There are three attributes; an {@code ID number}, a {@code name} for the account holder,
 * a {@code balance} (i.e. how much money is in the account). There is a public constructor to initialise these, and an
 * implementation of {@link BankAccount#toString toString()} to allow a pretty version of the object to be displayed.
 * 
 * <br /><br />
 * 
 * <b>New Concept</b>: you’ll see that the {@link BankAccount#toString toString()} method has the word {@literal @Override}
 * above it. This is called an <i>annotation</i>. Annotations are used to tell the Java compiler a little more about your
 * intentions when writing the code. This one is saying that {@link BankAccount#toString toString()} should override a
 * method inherited from a superclass. “Wait,” I hear you say, “BankAccount” doesn’t ‘extend’ anything!”. Ah, but it
 * does: by default, all Java objects extend the “{@link java.lang.Object Object}” class that comes included with Java, and
 * this defines several methods, including {@link java.lang.Object#toString() toString()},
 * {@link java.lang.Object#equals equals()} and {@link java.lang.Object#hashCode() hashCode}. The
 * {@link java.lang.Override Override} annotation is really useful because it tells the compiler to check that a
 * method really does override something: if you accidentally typed {@code tostring()} (try it!) then you would have
 * made an all-new method, and not overridden the behaviour of {@code Object.toString()}; the annotation means that the
 * compiler would generate an error to help you spot the bug.
 * 
 * <br /><br />
 * 
 * Now take a look at the {@link Experiment1#Experiment1 Experiment1} class: it only contains a main method, divided into
 * three parts. First it initialises some values, including a Random Number Generator (RNG). Then it creates and fills an
 * array with new {@link BankAccount#BankAccount BankAccount} objects. Finally, it loops over this array and prints
 * summaries of the objects. Take a look and try to understand what is happening at each step. What would you expect to see
 * in the output?
 * 
 * <br /><br />
 * 
 * <b>New Concept</b>: We want to generate a lot of data at random for our experiments, but computers like precise
 * instructions and aren’t very good at doing random things. A RNG uses some maths to generate a sequence of numbers
 * that look random (actually, this is called pseudo-random). The sequence is completely predictable given a particular
 * starting number called a seed, but is close enough to “random” for most purposes. The seed is set like this:
 * 
 * <pre>new Random(1)</pre>
 * 
 * and you’ll see that, if you try changing the number, you can make different lists of
 * {@link BankAccount#BankAccount BankAccount objects}.
 * 
 * <br /><br />
 * 
 * <b>New Concept</b>: You’ll see two kinds of {@code for(...)} loops here. Imagine there are a list of things; this
 * could be an array, a queue, a stack, or many other different Java types. If we wanted to do something with each element
 * in the list we’d have to have code like this:
 * 
 * <pre>
 * for (int i = 0; i {@literal <} list.length; i++) {
 *     System.out.println(list[i]);
 * }
 * </pre>
 * 
 * This is like the first loop we have in the {@link Experiment1#Experiment1 Experiment1} class. The following code is an
 * alternative for looping that is a little bit easier to read. For lists of Strings:
 * 
 * <pre>
 * for (String item : list) {
 *     System.out.println(item);
 * }
 * </pre>
 * 
 * Much nicer! This is a Java for-each loop. It is used only for looping through each element in a
 * {@link java.util.Collection Collection}. (We can read the above as, “for each String item in list”). Broken down we have:
 * 
 * <pre>
 * for ([type of thing in list] [nice readable name of a
 *     single thing in the list] : [the list name])
 * </pre>
 * 
 * This is the second loop that’s used, when we just want to look at all the accounts in turn. Now try running
 * {@link Experiment1#Experiment1 Experiment1}. Is the output as you expected?
 * 
 * <p><b>HashSets</b></p>
 * 
 * We’re now going to make use of a HashSet, a class provided as part of Java {@code Collections}. You can see the
 * documentation for this <a href="https://docs.oracle.com/javase/8/docs/api/java/util/HashSet.html">here</a>. A HashSet
 * is a hashtable implementation of a “set” - a collection in which no duplicate objects are allowed (a duplicate is
 * defined as being when {@code object1.equals(object2)}). Items that are added are grouped into buckets using their
 * hashcode, and checked against existing items in the HashSet. Attempting to add an item that is already stored will
 * do nothing. This is described in the documentation for the Set interface
 * <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Set.html#add(E)">here</a>.
 * 
 * <br /><br />
 * 
 * There is a large block of commented-out code in the {@link Experiment1#Experiment1 Experiment1} class. Uncomment this.
 * It will create another array of {@link BankAccount#BankAccount BankAccount} objects. You’ll see that it creates a new
 * RNG using the same seed as before, so these objects are identical to those in the first array, in that they will have
 * the same IDs, names and balances. You can check this by changing the print loop at the bottom of the class to iterate
 * over {@code accounts2}.
 * 
 * <br /><br />
 * 
 * We then create a {@link java.util.HashSet HashSet}, and use a for loop fill it with the
 * {@link BankAccount#BankAccount BankAccount} objects in
 * the original accounts array.
 * 
 * <br /><br />
 * 
 * <b>New Concept</b>: Java Generics. You’ll see that after HashSet is {@code <BankAccount>}. This tells Java that you want
 * a {@code HashSet} for containing {@link BankAccount#BankAccount BankAccount} objects. You don’t need to worry about the
 * details here, but it makes for much cleaner code and again gives the compiler more information about your intentions.
 * 
 * <br /><br />
 * 
 * So, we now have a {@code HashSet} filled with {@code BankAccount} objects. Let’s use our array of identical
 * {@code BankAccount} objects to check whether the {@code HashSet} contains them. Make sure that the final “print
 * summaries” loop is over {@code accounts2}. Then, within the loop, after the existing
 * {@link java.lang.System System.out.println()}, add this:
 * 
 * <pre>System.out.println(accountSet.contains(ba));</pre>
 * 
 * This will print true or false for each of the accounts. What values do you expect to see here?
 * 
 * <br /><br />
 * 
 * The reason we are seeing this is that we haven’t overridden {@link java.lang.Object#equals equals()} for our
 * {@code BankAccount} objects. In the {@link java.lang.Object Object} class this method only returns true if the
 * objects are the same object, not just objects containing the same values. So, when the {@code HashSet} checks the
 * {@code accounts2} objects against the objects that came from {@code accounts} it always comes up as false. They are the
 * same in one sense, but different in another!
 * 
 * <br /><br />
 * 
 * 
 * To fix this, let’s implement {@link java.lang.Object#equals equals()} in {@link BankAccount#BankAccount BankAccount}. An
 * implementation is provided, you can just uncomment it. {@code BankAccount} objects are now regarded as equal as long
 * as they have the same ID number. Re-run {@link Experiment1#Experiment1 Experiment1}. What do you expect to see now?
 * 
 * <br /><br />
 * 
 * Okay, there is another problem! All those {@link BankAccount#BankAccount BankAccount} objects are going missing. This
 * might seem like the basis of a money-making scam, but let’s stay on the right side of the law. Let’s look at the
 * documentation for {@link java.lang.Object#equals equals()}.
 * 
 * <br /><br />
 * 
 * <q><i>
 * Note that it is generally necessary to override the {@link java.lang.Object#hashCode() hashCode} method whenever this
 * method is overridden, so as to maintain the general contract for the hashCode method, which states that equal objects
 * must have equal hash codes.
 * </i></q>
 * 
 * <br /><br />
 * 
 * What has happened is that the objects are still using the {@code hashCode()} implementation from the {@code Object}
 * class, but are now using the {@code equals()} method we’ve added to {@code BankAccount}. We’ve broken the contract
 * specified in the documentation! This means that equal objects are getting different hashcodes, and so are ending up
 * in different buckets in the hashtable inside {@code HashSet}. Given an object, we don’t look at the whole hashtable
 * for matching objects, we just look in the appropriate bucket. So, we look in the wrong place and don’t see the
 * matching objects.
 * 
 * <br /><br />
 * 
 * Let’s fix {@link BankAccount#hashCode hashCode()} so it is based on the same variable as
 * {@link BankAccount#equals equals()}. Uncomment the {@code hashCode} implementation. Rerun
 * {@link Experiment1#Experiment1 Experiment1}. What happens now?
 * 
 * <br /><br />
 * 
 * Try changing the RNG seed for the second array of {@link BankAccount#BankAccount BankAccount} objects – what happens?
 * 
 * <br /><br />
 * 
 * So, now we have the basics. We can create a lot of {@link BankAccount#BankAccount BankAccount} objects, and put them into
 * a {@link java.util.HashSet HashSet}. Let’s see what the impact of getting a good hashing function is.
 * {@link Experiment2#Experiment2 Experiment2} contains the code for a simple experiment. It will generate a large number
 * of {@code BankAccount} objects and put them into a {@code HashSet}. It then generates another large number of
 * {@code BankAccount} objects; half of them are equal to the first ones, and half of then are new. It measures the time
 * taken to check whether or not these are already in the {@code HashSet}. This is divided by the number of objects, so the
 * number displayed on the console is how long, per object, the checking takes. We might expect the time to check each
 * object to go up as the {@code HashSet} has more objects in it – what do you think will happen? Open up Excel – we are
 * going to record just how slow this thing can get.
 * 
 * <ol>
 *     <li>
 *     Try running {@link Experiment2#Experiment2 Experiment2} and recording the values of N and the time per object.
 *     </li>
 *     <li>
 *     Double the value of {@code numberOfAccounts}, run {@code Experiment2} again and record the values.
 *     </li>
 *     <li>
 *     Do this a few times – you’ll find the experiment takes longer because creating a larger number of objects takes
 *     more time. You might find that making more than about 8,000,000 objects will be your limit
 *     (possibly due to memory limitations).
 *     </li>
 *     <li>
 *     Make a plot of time vs N. What trend do you see?
 *     </li>
 * </ol>
 * 
 * Image we had implemented {@code hashCode()} poorly and it tended to give the same value to all objects. We can
 * simulate this by just making {@link BankAccount#hashCode BankAccount.hashCode()} always return a zero.
 * (this doesn’t break the contract, because all objects that are equal to each other will also have the same hashcode!)
 * Repeat the experiment above. What trend do you see now?
 * 
 * <p><b>HashMaps and more...</b></p>
 * 
 * In your own time, you can take a look at the demo of {@link java.util.HashMap HashMap} in {@code Experiment4}. They are
 * an incredibly useful structure: they store “key,value” pairs, and allow objects (values) to be stored using another
 * object as a reference (keys). The “key” objects might be simple objects like {@code Strings} or {@code Integers}, or
 * could be more complex objects. A common use-case is where the keys are some kind of IDs (perhaps from a database query),
 * and each value is an {@link java.util.ArrayList ArrayList} holding the fields associated with that ID. The keys of a
 * {@code HashMap} are actually stored in a hashtable. There is also a {@link java.util.TreeMap TreeMap} (and a
 * {@link java.util.TreeSet TreeSet}) where the keys are stored in a binary tree; with these the “key” objects need to
 * implement {@link java.lang.Comparable Comparable} (that is, have a {@code compareTo()} method), fortunately many
 * commonly used classes for “key” objects implement this – including {@code String} and {@code Integer}.
 */
package practical6;