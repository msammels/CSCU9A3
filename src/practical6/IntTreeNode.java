package practical6;

/**
 * CSCU9A3 - Practical 6 <br />
 * <code>IntTreeNode.java</code>
 *
 * @author  Michael Sammels
 * @version 29.10.2020
 * @since   1.0
 */

public class IntTreeNode {
    /**
     * The value.
     */
    private final int value;
    
    /**
     * The left node.
     */
    private IntTreeNode left = null;
    
    /**
     * The right node.
     */
    private IntTreeNode right = null;

    /**
     * Default constructor that initialises the node with a value for this node.
     * @param v the value for the node to store.
     */
    public IntTreeNode(int v) {
        value = v;
    }

    /**
     * Set the left reference of this node to 'n'.
     * @param n a reference to the new left node.
     */
    public void setLeft(IntTreeNode n) {
        left = n;
    }

    /**
     * Set the right reference of this node to 'n'.
     * @param n a reference to the new right node.
     */
    public void setRight(IntTreeNode n) {
        right = n;
    }

    // Get properties of the node
    
    /**
     * Get the nodes value.
     * @return The value of the node.
     */
    public int getValue() {
        return value;
    }

    /**
     * Direction.
     * @return Is it left?
     */
    public boolean hasLeft() {
        return left != null;
    }

    /**
     * Direction.
     * @return Is it right?
     */
    public boolean hasRight() {
        return right != null;
    }

    /**
     * Get the left node.
     * @return The left node.
     */
    public IntTreeNode left() {
        return left;
    }

    /**
     * Get the right node.
     * @return The right node.
     */
    public IntTreeNode right() {
        return right;
    }
    
    /**
     * Make the output look pretty.
     */
    public String toString() {
        return String.format("%d", value);
    }
}
