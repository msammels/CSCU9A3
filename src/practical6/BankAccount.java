package practical6;

/**
 * CSCU9A3 - Practical 6 <br />
 * <code>BankAccount.java</code>
 *
 * @author  Michael Sammels
 * @version 29.10.2020
 * @since   1.0
 */

public class BankAccount {
    /**
     * The customers ID number.
     */
    private final int idNumber;
    
    /**
     * The customers name.
     */
    private final String name;
    
    /**
     * The customers balance.
     */
    private final double balance;

    /**
     * Method to take in details.
     * @param name              the customers name.
     * @param idNumber          the customers ID number.
     * @param initialBalance    the customers initial balance.
     */
    public BankAccount(int idNumber, String name, double initialBalance) {
        this.idNumber = idNumber;
        this.name = name;
        this.balance = initialBalance;
    }

    @Override
    public String toString() {
        return "Account [ID=" + idNumber + ", name=" + name + ", balance=£" + balance + "]";
    }

    @Override
    public boolean equals(Object obj) {
        return this.idNumber == ((BankAccount) obj).idNumber;
    }

    @Override
    public int hashCode() {
        // We could just return idNumber here as it's already an int but this is to illustrate that we can do other
        // things to produce a valid hashCode, which must always be an int.
        return Integer.hashCode(this.idNumber);
    }
}
