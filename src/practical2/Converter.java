package practical2;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * CSCU9A3 - Practical 2 <br />
 * <code>Converter.java</code>
 * 
 * Converter class which uses Colin's top secret rules to calculate the estimated duration of a cycle given
 * certain information about the cyclist, the route and the weather.
 *
 * @author  Michael Sammels
 * @version 07.10.2020
 * @since   1.0
 */

public class Converter {
    /**
     * Miles per hour to kilometers per hour.
     * @param mph   miles per hour.
     * @return      The conversion.
     */
    public double mph2kph(double mph)   { return mph * 1.609; }
    
    /**
     * Celsius to Fahrenheit.
     * @param tmp   the temporary temperature.
     * @return      The conversion.
     */
    public double c2f(double tmp)       { return celsius_to_fahrenheit(tmp); }
    
    /**
     * Pounds to Dollars.
     * @param mon   the monetary value.
     * @return      The conversion.
     */
    public double gbp2usd(double mon)   { return pounds_to_dollars(mon); }
    
    /**
     * Seconds to hours.
     * @param sec   the seconds.
     * @return      The conversion.
     */
    public double sec2hrs(double sec)   { return sec / 3600; }

    /**
     * Miles per hour to kilometers per hour.
     * @param mph   the speed in miles per hour.
     * @return      The speed in kilometers per hour.
     */
    public String mph2kph_printing(double mph) {
        // The DecimalFormat is needed to configure the precision (number of decimals) of the double variable when
        // converting to string
        return mph + " mph = " + new DecimalFormat("#.#",
                DecimalFormatSymbols.getInstance(Locale.ENGLISH)).format(mph2kph(mph)) + " kph";
    }

    /**
     * Compare miles per hour, with kilometres per hour.
     * @param mph1  the first value.
     * @param mph2  the second value.
     * @return      The comparison.
     */
    public boolean mph2kph_compare(double mph1, double mph2) { return mph2kph(mph1) >= mph2kph(mph2); }
    
    /**
     * Convert Celsius to Fahrenheit.
     * @param celsius   the temperature in celsius.
     * @return          The temperature in fahrenheit.
     */
    public double celsius_to_fahrenheit(double celsius) { return (celsius * 9.0 / 5.0) + 32.0; }

    /**
     * Convert Pounds to Dollars.
     * @param pounds    the monetary value in Pounds.
     * @return          The value in Dollars.
     */
    public double pounds_to_dollars(double pounds) {
        double rate = 0.77;
        return pounds * rate;
    }

    /**
     * Convert the array.
     * @param type      the type of conversion.
     * @param values    the values.
     * @return          The conversion.
     */
    public double[] convert_array(String type, double[] values) {
        double[] out_values = new double[values.length];

        // Converting mph2kph
        if (type.equals("mph2kph")) {
            for (int i = 0; i < values.length; i++) {
                out_values[i] = mph2kph(values[i]);
            }
        }

        // Converting celsius to fahrenheit
        if (type.equals("c2f")) {
            for (int i = 0; i < values.length; i++) {
                out_values[i] = c2f(values[i]);
            }
        }

        // Converting pounds to dollars
        if (type.equals("gbp2usd")) {
            for (int i = 0; i < values.length; i++) {
                out_values[i] = gbp2usd(values[i]);
            }
        }

        // Converting seconds to hours
        if (type.equals("sec2hrs")) {
            for (int i = 0; i < values.length; i++) {
                out_values[i] = sec2hrs(values[i]);
            }
        }
        return out_values;
    }
}
