package practical2;

import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * CSCU9A3 - Practical 2 <br />
 * ConverterTest.java
 *
 * @author  Michael Sammels
 * @version 07.10.2020
 * @since   1.0
 */

public class ConverterTest {
    /**
     * The accuracy we are looking for.
     */
    private static final double ACCURACY = 0.05;
    
    /**
     * A new Converter object.
     */
    private static Converter converter;

    /**
     * This method only setups the unit testing. In other words, this method is responsible for instantiating or
     * initialising any variables that unit test methods need. There are different annotations as can be seen
     * <a href="https://junit.org/junit5/docs/current/user-guide/">here</a> and also {@link org.junit.jupiter here}.
     */
    @BeforeAll
    public static void setup() {
        converter = new Converter();
    }

    /**
     * Test converting from miles per hour to kilometers per hour - comparing doubles using
     * {@link org.junit.Assert#assertEquals assertEquals}.
     */
    @Test
    public void test_mph2kph() {
        assertEquals("mph2kph failed:", 38.6243, converter.mph2kph(24), ACCURACY);
        assertEquals("mph2kph failed: ", 96.5606, converter.mph2kph(60), ACCURACY);
    }

    /**
     * Test printing miles per hour to kilometers per hour - comparing Strings using
     * {@link org.junit.Assert#assertEquals assertEquals}.
     */
    @Test
    public void test_mph2kph_printing() {
        assertEquals("mph2kph printing failed: ", "60.0 mph = 96.5 kph", converter.mph2kph_printing(60));
    }

    /**
     * Test comparison between two miles per hour values - comparing integers using
     * {@link org.junit.Assert#assertFalse(boolean condition) assertFalse}.
     */
    @Test
    public void test_mph2kph_compare() {
        assertFalse("mph2km comparison failed: ", converter.mph2kph_compare(24, 60));
    }

    /**
     * Test comparison between two temperature values - comparing doubles using
     * {@link org.junit.Assert#assertEquals assertEquals}.
     */
    @Test
    public void test_celsius_to_fahrenheit() {
        assertEquals("Celsius to Fahrenheit failed: ", 53.6, converter.celsius_to_fahrenheit(12.0), ACCURACY);
    }

    /**
     * Test comparison between two monetary values - comparing doubles using
     * {@link org.junit.Assert#assertEquals assertEquals}.
     */
    @Test
    public void test_pounds_to_dollars() {
        assertEquals("Pounds to Dollars failed: ", 23.1, converter.pounds_to_dollars(30), ACCURACY);
    }

    /**
     * Test comparison between two time values - comparing doubles using
     * {@link org.junit.Assert#assertEquals assertEquals}.
     */
    @Test
    public void test_seconds_to_hours() {
        assertEquals("Seconds to hours failed: ", 2.73, converter.sec2hrs(10000), ACCURACY);
    }

    /**
     * Test comparison between arrays - comparing integers using
     * {@link org.junit.Assert#assertArrayEquals asserArraytEquals}.
     */
    @Test
    public void test_convert_array() {
        /**
         * mph2kph - expected result.
         */
        double[] mph2kph_expected = new double[] {converter.mph2kph(24), converter.mph2kph(60)};
        
        /**
         * mph2kph - actual result.
         */
        double[] mph2kph_actual = converter.convert_array("mph2kph", new double[] {24, 60});

        /**
         * c2f - expected result.
         */
        double[] c2f_expected = new double[] {converter.c2f(30)};
        
        /**
         * c2f - actual result.
         */
        double[] c2f_actual = converter.convert_array("c2f", new double[] {30});

        /**
         * gbp2usd - expected result.
         */
        double[] gbp2usd_expected = new double[] {converter.gbp2usd(30)};
        
        /**
         * gbp2usd - actual result.
         */
        double[] gbp2usd_actual = converter.convert_array("gbp2usd", new double[] {30});

        /**
         * sec2hrs - expected result.
         */
        double[] sec2hrs_expected = new double[] {converter.sec2hrs(10000)};
        
        /**
         * sec2hrs - actual result.
         */
        double[] sec2hrs_actual = converter.convert_array("sec2hrs", new double[] {10000});

        assertArrayEquals("mph2kph_expected failed: ",  mph2kph_expected, mph2kph_actual, ACCURACY);
        assertArrayEquals("c2f_expected failed: ",      c2f_expected, c2f_actual, ACCURACY);
        assertArrayEquals("gbp2usd_expected failed: ",  gbp2usd_expected, gbp2usd_actual, ACCURACY);
        assertArrayEquals("sec2hrs_expected failed: ",  sec2hrs_expected, sec2hrs_actual, ACCURACY);
    }
}
