package practical7;

/**
 * CSCU9A3 - Practical 7 <br />
 * <code>BankAccount.java</code>
 *
 * @author  Michael Sammels
 * @version 11.11.2020
 * @since   1.0
 */

public class BankAccount implements Comparable<BankAccount> {
    
    /**
     * The customers ID number.
     */
    private final int idNumber;
    
    /**
     * The customers name.
     */
    private final String name;
    
    /**
     * The customers balance.
     */
    private final double balance;

    /**
     * Set the customer details.
     * @param idNumber          the customers ID number.
     * @param name              the customers name.
     * @param initialBalance    the initial balance.
     */
    public BankAccount(int idNumber, String name, double initialBalance) {
        this.idNumber = idNumber;
        this.name = name;
        this.balance = initialBalance;
    }

    @Override
    public String toString() {
        return "Account [ID=" + idNumber + ", name=" + name + ", balance=£" + balance + "]";
    }

    @Override
    public int compareTo(BankAccount o) {
        return Double.compare(balance, o.balance);
    }
}
