/**
 * <p><b>Sorting</b></p>
 * 
 * <p><b>Task 1: Sorting Data</b></p>
 * 
 * For the first part of the practical, we are going to look at implementing the selection sort algorithm and time how
 * long it takes to sort increasingly large data sets. In the directory {@code \\Wide\Groups\CSCU9A3\Practicals\Practical4},
 * you should find the files {@linkplain Sorting#Sorting Sorting.java} and
 * {@linkplain SortingTest#SortingTest SortingTest.java}. Create a new Eclipse project in your home file space and add
 * these files to it.
 * 
 * <br /><br />
 * 
 * The class {@code Sorting.java} contains just one method called {@link Sorting#selectionSort selectionSort} which takes
 * an array of integer values and is meant to sort it using the selection sort algorithm. Your first task is to implement
 * this method using the pseudo code given in lectures, then check that it works using the method {@code selectionSortTest}
 * that is provided in {@code SortingTest.java}. This test method introduces a new assertion test that you may have not
 * seen before called {@link org.junit.Assert#assertArrayEquals assertArrayEquals}. This test takes an expected array and
 * compares its contents with an array that you have produced.
 * 
 * <br /><br />
 * 
 * Once you have managed to get the {@code selectionSort} algorithm to work, your next task is to see how long it takes to
 * run for different sized data sets. A method called {@link SortingTest#sortForSize sortForSize} has been provided for
 * you in {@code SortingTest.java}. This method will create an array of a given size ‘n’, call your {@code selectionSort}
 * method and note how long it took to run in nano-seconds. The test method
 * {@link SortingTest#timeSelectionSort timeSelectionSort} has been provided which will make a single call to
 * {@code sortForSize} and print out the number of items sorted and how long it took.
 * 
 * <br /><br />
 * 
 * Add some additional calls to {@code sortForSize} with different sizes of data set and build up a list of items to sort
 * versus time taken. Use this data in Excel to plot the relationship between data set size and time to sort and confirm
 * the shape matches the N<sup>2</sup> computational order discussed in lectures.
 * 
 * Note: Do not try to sort more than 50,000 items – it will take increasingly longer to run and your computer may slow
 * down when large value of ‘n’ are used. To plot this data, use a scatter plot in Excel (with connecting lines if you
 * like). The value for ‘n’ should be on the ‘x’ axis and the time taken should be on the ‘y’ axis’.
 * 
 * <p><b>Task 2: Sorting using the build-in sorter, and <i>Comparable</i></b></p>
 * 
 * Look at {@link SortingExperiment#SortingExperiment SortingExperiment}. It is much the same as
 * {@link practical6.Experiment1 Experiment1} from last week, but the {@code BankAccount} objects are just placed straight
 * into an array. The loop at the start shows the order they are in. Wouldn’t it be useful to just sort these without
 * actually implementing a sort algorithm? Fortunately the Java developers have included a smart sorting algorithm
 * called TimSort<sup>[1]</sup> that will do this for you; implemented in
 * {@link java.util.Arrays#sort java.util.Arrays.sort()} for arrays, and
 * {@link java.util.Collections#sort java.util.Collections.sort} for lists.
 * 
 * All you have to do is provide a way for Java to know whether one object in a class is more-than, less-than or equal-to
 * another. We do this by having the class implement the {@link java.lang.Comparable Comparable} interface.
 * 
 * <br /><br />
 * 
 * There are two steps to doing this.
 * 
 * <ol>
 *     <li>
 *     Change the class declaration to:
 *     
 *     <pre>public class BankAccount implements Comparable{@literal <BankAccount>} {</pre>
 *     </li>
 *     <li>
 *     Depending on the IDE you’re using you might get a warning now that you need to implement the
 *     {@link java.lang.Comparable#compareTo compareTo()} method. If this happens you can usually click on a button to
 *     generate a template for this method, or you can type it yourself. The template looks like this:
 *     
 *     <pre>
 *     public int compareTo(BankAccount that) {
 *         return ...;
 *     }
 *     </pre>
 *     </li>
 * </ol>
 * 
 * Your job is to implement the body of this method, so that bank accounts are sorted by their balance. The method should
 * return a negative number if “this” account has a smaller balance than “that”; it should return zero if the two balances
 * are equal, and it should return a positive number if “this” has a larger balance than “that”. Once you’re happy with
 * your implementation, go back to the {@link practical6.Experiment3} class, and uncomment the {@code Arrays.sort()} line.
 * Run the program – are the bank accounts sorted in ascending order of balance?
 * 
 * <p><sup>
 * 1: Incidentally, TimSort is only used for objects. For arrays of primitives like int and double, a variant of QuickSort
 * is used.
 * </sup></p>
 */
package practical7;