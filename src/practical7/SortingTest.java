package practical7;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

/**
 * CSCU9A - Practical 7 <br />
 * <code>SortingTest.java</code>
 *
 * @author  Michael Sammels
 * @version 11.11.2020
 * @since   1.0
 */

public class SortingTest {
    /**
     * A test for the selection sort.
     */
    @Test
    public void selectionSortTest() {
        int[] data = {5, 3, 7, 1, 9, 4, 2};
        int[] expected = {1, 2, 3, 4, 5, 7, 9};

        Sorting s = new Sorting();

        s.selectionSort(data);

        for (int value : data) System.out.print(value + ",");

        System.out.println();

        assertArrayEquals(expected, data);
    }
    
    /**
     * Sort for the size of the array.
     * @param n the size to be sorted.
     * @return  The sorted array.
     */
    public long sortForSize(int n) {
        // Create an array to hold a data set of size 'n'
        int[] data = new int[n];

        // Fill up our array with random numbers
        for (int i = 0; i < n; i++) data[i] = (int) (Math.random() * 100000.0f);

        Sorting s = new Sorting();

        // Now sort them and time how long it takes
        return s.selectionSort(data);
    }

    // Uncomment the @Test line when you are ready to run this test
    
    /**
     * Test how long it takes to run the selection sort.
     */
    @Test
    public void timeSelectionSort() {
        long time;
        int size = 100;

        time = sortForSize(size);

        // Print out the data set size against the time taken in milliseconds
        System.out.printf("%d\t%.3f%n", size, time / 1000000.0f);
    }
}
