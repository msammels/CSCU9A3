package practical7;

/**
 * CSCU9A3 - Practical 7 <br />
 * <code>Sorting.java</code>
 *
 * @author  Michael Sammels
 * @version 11.11.2020
 * @since   1.0
 */

public class Sorting {
    /**
     * The selection sort algorithm.
     * @param data the data to sort.
     * @return      The sorted data.
     */
    public long selectionSort(int[] data) {
        // Note the time at the start
        long startTime = System.nanoTime();

        // Your implementation code should go here...
        for (int i = 0; i < data.length - 1; i++) {
            int min = i;

            for (int j = i + 1; j < data.length; j++) {
                if (data[min] > data[j]) {
                    min = j;
                }
            }

            if (min != i) {
                int temp = data[i];
                data[i] = data[min];
                data[min] = temp;

            }
        }

        // Return how much time has elapsed to do this task
        return System.nanoTime() - startTime;
    }
}
