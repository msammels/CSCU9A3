package practical3;

/**
 * CSCU9A3 - Practical 3 <br />
 * <code>MountainBikeRide.java</code>
 *
 * @author  Michael Sammels
 * @version 08.10.2020
 * @since   1.0
 */

public class MountainBikeRide extends CycleRide {
    /**
     * Superclass for the mountain bike selection of cycle ride.
     * @param numberMiles   the number of miles.
     * @param cycleAlone    are they travelling alone or not?
     */
    public MountainBikeRide(int numberMiles, boolean cycleAlone) {
        super(numberMiles, cycleAlone);
    }

    /**
     * Get the difficulty factor.
     * @return The difficulty factor.
     */
    public double getDifficultyFactor() {
        return 1.5;
    }
}
