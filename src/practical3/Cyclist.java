package practical3;

/**
 * CSCU9A3 - Practical 3 <br />
 * <code>Cyclist.java</code>
 *
 * @author  Michael Sammels
 * @version 08.10.2020
 * @since   1.0
 */

public class Cyclist {
    /**
     * The cyclists competency level.
     */
    private final String competency;
    
    /**
     * How many years of experience the cyclist has.
     */
    private final int numYearsExperience;

    /**
     * Get the cyclist details.
     * @param competency            the cyclists competency.
     * @param numYearsExperience    the number of years of experience.
     */
    public Cyclist(String competency, int numYearsExperience) {
        this.competency = competency;
        this.numYearsExperience = numYearsExperience;
    }

    /**
     * Experience.
     * @return  The number of years of experience.
     */
    public int getNumYearsExperience() {
        return numYearsExperience;
    }

    /**
     * Difficulty level.
     * @return  True, if the cyclist is a beginner.
     */
    public boolean isBeginner() {
        return competency.equalsIgnoreCase("Beginner");
    }

    /**
     * Difficulty level.
     * @return  True, if the cyclist is an intermediate.
     */
    public boolean isIntermediate() {
        return competency.equalsIgnoreCase("Intermediate");
    }

    /**
     * Difficulty level.
     * @return  True, if the cyclist is advanced.
     */
    public boolean isAdvanced() {
        return competency.equalsIgnoreCase("Advanced");
    }
}
