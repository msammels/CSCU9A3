/**
 * <p><b>Cycle Ride Calculator - Objectified!</b></p>
 * 
 * <p><b>Intro</b></p>
 * 
 * In this practical class, we are going to study how to create classes, instantiate objects, and use them as parameters.
 * 
 * <p><b>Getting Started</b></p>
 * 
 * Remember how we had this giant method:
 * 
 * <pre>
 * public double getDuration(int numMiles, String competency, int numYearsExperience, boolean cyclingAlone,
 *                           int temp, int windSpeed, boolean isRaining)
 * </pre>
 * 
 * There are rather too many parameters. How could we group some of this information together to result in fewer
 * parameters, but the same information being passed?
 * 
 * <br /><br />
 * 
 * One of the best ways to do this is to use the concepts of classes and objects. For instance, suppose we need to program
 * a data structure to store the name, registration number, and username of all the students of the university. We could
 * group this information together as a Student class that would look like the following:
 * 
 * <p><img src="{@docRoot}/resources/practical3-img01.jpg" alt ="practical3-img01" height="25%" width="25%" /></p>
 * 
 * This information is encapsulated within the class, so instead of passing the name, registration number and username
 * separately, we could just pass a Student object instead.
 * 
 * <p><b>Weather</b></p>
 * 
 * Copy the code from:
 * 
 * <p>{@code Groups On Wide > CSCU9A3 > Practicals > Practical 3}</p>
 * 
 * Remember that if you are accessing {@code Groups On Wide} from outside the university, you will need to be connected to
 * the VPN. More information on how to configure/use the VPN can be found
 * 
 * <a href="https://stir.unidesk.ac.uk/tas/public/ssp/content/detail/knowledgeitem?unid=f2963fcee9b644c883e0aa443d10bb43">here</a>
 * 
 * Notice that this code is using this concept of classes and objects. Run the tests and ensure that they pass. Now take a
 * look at the code and explore what has changed since last week.
 * 
 * <br /><br />
 * 
 * We are now encapsulating the ride and the cyclist attributes in the {@link CycleRide#CycleRide CycleRide} and
 * {@link Cyclist#Cyclist Cyclist} classes, respectively. Both are these classes are used as parameters, instead of using
 * several separate parameters as before. Much better!
 * 
 * <br /><br />
 * 
 * As you can see, we have some parameters that would naturally lend themselves to being encapsulated by a
 * <b>Weather class</b>. Make it so!
 * 
 * <br /><br />
 * 
 * Amend the remaining code (including the {@link CycleCalculatorTest#CycleCalculatorTest CycleCalculatorTest}) where needed.
 * 
 * <p><b>Inheritance</b></p>
 * 
 * Imagine that there could be different types of {@link CycleRide#CycleRide CycleRide}. For example, you could go mountain
 * biking or road racing. Each would have different difficulty factors. For example, a road route would have a small
 * difficulty factor, whereas a mountain bike route would have a relatively high difficulty factor.
 * <b><i>Instead of storing the difficulty factor as a variable</i></b> on {@link CycleRide#CycleRide CycleRide}, design
 * different subclasses of {@link CycleRide#CycleRide CycleRide} that can return a value suitable for that type of ride.
 * Diagrammatically, this might be:
 * 
 * <p><img src="{@docRoot}/resources/practical3-img02.jpg" alt ="practical3-img02" height="50%" width="50%" /></p>
 * 
 * The method to return the difficulty factor should return 1 for a generic {@code CycleRide}, 1.5 for a mountain bike
 * ride, and 0.5 for a road bike ride. Implement {@link MountainBikeRide#MountainBikeRide MountainBikeRide} and
 * {@link RoadBikeRide#RoadBikeRide RoadBikeRide}, inheriting from {@link CycleRide#CycleRide CycleRide}. This will most
 * likely require you to refer to notes, examples, the Internet, books. Do what it takes to get it all working. Ask for
 * help if you really get stuck!
 * 
 * <br /><br />
 * 
 * Note that you would also probably add more new features (methods and attributes) that would be specific to your new
 * types of {@link CycleRide#CycleRide CycleRide} that would differentiate them further from the base {@code CycleRide}.
 * The general rule with inheritance is that the base class contains the core functionality and the sub-classes extend this
 * functionality to provide their own specific behaviour.
 * 
 * <br /><br />
 * 
 * After implementing the subclasses ({@link MountainBikeRide#MountainBikeRide MountainBikeRide} and
 * {@link RoadBikeRide#RoadBikeRide RoadBikeRide}), change your {@link CycleCalculator#getDuration getDuration} method so it
 * multiplies the calculated duration with the difficulty (retrieved from the
 * {@link CycleRide#getDifficultyFactor getDifficultyFactor} method). Check your implementation is working by using the
 * {@link CycleCalculatorAdvancedTest#CycleCalculatorAdvancedTest CycleCalculatorAdvancedTest}. Note that this advanced
 * test class doesn’t use your recently created {@link Weather#Weather Weather} class. Amend this test class so it uses
 * your {@link Weather#Weather Weather} class. Furthermore, the advanced test
 * class only tests the {@link CycleRide#CycleRide CycleRide} and the
 * {@link MountainBikeRide#MountainBikeRide MountainBikeRide}. Amend this class and include a new test for the
 * {@link RoadBikeRide#RoadBikeRide RoadBikeRide}.
 */
package practical3;