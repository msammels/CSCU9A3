package practical3;

/**
 * CSCU9A3 - Practical 3 <br />
 * <code>CycleRide.java</code>
 *
 * @author  Michael Sammels
 * @version 08.10.2020
 * @since   1.0
 */

public class CycleRide {
    /**
     * The number of miles they will travel.
     */
    private final int numberMiles;
    
    /**
     * Are the cycling alone or not?
     */
    private final boolean cycleAlone;

    /**
     * Get the relevant details.
     * @param numberMiles   the number of miles of the trip.
     * @param cycleAlone    if they are cycling alone or not.
     */
    public CycleRide(int numberMiles, boolean cycleAlone) {
        this.numberMiles = numberMiles;
        this.cycleAlone = cycleAlone;
    }

    /**
     * Mileage.
     * @return  How long is the trip?
     */
    public int getNumberMiles() {
        return numberMiles;
    }

    /**
     * Company.
     * @return  Are they cycling alone?
     */
    public boolean isCycleAlone() {
        return cycleAlone;
    }

    /**
     * Experience.
     * @return  How difficult the journey will be on their chosen bike.
     */
    public double getDifficultyFactor() {
        return 1.0;
    }
}
