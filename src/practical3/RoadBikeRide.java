package practical3;

/**
 * CSCU9A3 - Practical 3 <br />
 * <code>RoadBikeRide.java</code>
 *
 * @author  Michael Sammels
 * @version 08.10.2020
 * @since   1.0
 */

public class RoadBikeRide extends CycleRide {
    /**
     * Constructor.
     * @param numberMiles   the number of lines.
     * @param cycleAlone    are they cycling with a friend?
     */
    public RoadBikeRide(int numberMiles, boolean cycleAlone) {
        super(numberMiles, cycleAlone);
    }

    /**
     * Get the difficulty factor.
     * @return The difficulty factor.
     */
    public double getDifficultyFactor() {
        return 0.5;
    }
}
