package practical3;

/**
 * CSCU9A3 - Practical 3 <br />
 * <code>Weather.java</code>
 *
 * @author  Michael Sammels
 * @version 08.10.2020
 * @since   1.0
 */

public class Weather {
    /**
     * The temperature.
     */
    protected int temp;
    
    /**]
     * The wind speed.
     */
    protected int windSpeed;
    
    /**
     * Is it raining or not?
     */
    protected boolean isRaining;

    /**
     * Constructor.
     * @param t     the temperature.
     * @param wd    the wind speed.
     * @param r     is it raining?
     */
    public Weather(int t, int wd, boolean r) {
        this.temp = t;
        this.windSpeed = wd;
        this.isRaining = r;
    }

    /**
     * Get the temperature.
     * @return The temperature.
     */
    public int getTemp() {
        return temp;
    }

    /**
     * Get the wind speed.
     * @return The wind speed.
     */
    public double getWindSpeed() {
        return windSpeed;
    }

    /**
     * Check if it is raining.
     * @return True if it is raining, false otherwise.
     */
    public boolean getIsRaining() {
        return isRaining;
    }
}
