package assignment;

/**
 * CSCU9A3 Assignment <br />
 * <code>MScStudent.java</code>
 *
 * @author  2629330
 * @version 01.11.2020
 * @since   1.0
 */

public class MScStudent extends Student {
    /**
     * The research title of the {@link MScStudent}.
     */
    protected String researchTitle;
    
    /**
     * The {@link Professor} supervisor of the student.
     */
    protected Professor supervisor;

    /**
     * Constructor.
     * @param fn    name.
     * @param rn    registration number.
     * @param email email.
     * @param rt    research title.
     * @param s     supervisor.
     */
    public MScStudent(String fn, int rn, String email, String rt, Professor s) {
        super(fn, rn, email);
        this.researchTitle = rt;
        this.supervisor = s;
    }

    /**
     * Make the output pretty.
     */
    public String toString() {
        return "MScStudent: " + fullName + " - StudentNo: " + registrationNumber + " - Research Title: "
                + researchTitle + " - Supervisor: " + supervisor;
    }
}
