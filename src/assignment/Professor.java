package assignment;

/**
 * CSCU9A3 Assignment <br />
 * <code>Professor.java</code>
 *
 * @author  2629330
 * @version 01.11.2020
 * @since   1.0
 */

public class Professor implements Person {
    /**
     * The professors full name.
     */
    protected String fullName;
    
    /**
     * The professors registration number.
     */
    protected int registrationNumber;

    /**
     * Assign the variable as required.
     * @param fn                    the full name.
     * @param registrationNumber    the registration number.
     * @param email                 the email.
     */
    public Professor(String fn, int registrationNumber, String email) {
        this.fullName = fn;
        this.registrationNumber = registrationNumber;
    }

    /**
     * Get the professor's name.
     * @return The full name.
     */
    public String getFullName() { return fullName; }
    
    /**
     * Set the full name.
     * @param fn full name.
     */
    public void setFullName(String fn) { this.fullName = fn; }

    /**
     * Get the professors registration number.
     * @return The registration number.
     */
    public int getRegistrationNumber() { return registrationNumber; }
    
    /**
     * Set the registration number.
     * @param rn The registration number.
     */
    public void setRegistrationNumber(int rn) { this.registrationNumber = rn; }
    
    /**
     * Make the output look pretty.
     */
    @Override
    public String toString() { return "Professor: " + fullName; }
}
