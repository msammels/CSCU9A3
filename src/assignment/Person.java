package assignment;

/**
 * CSCU9A3 Assignment <br />
 * <code>Person.java</code>
 *
 * <p>Interface that implements some methods common to all persons represented in the system.</p>
 *
 * @author  2629330
 * @version 01.11.2020
 * @since   1.0
 */

interface Person {
    /**
     * Get the person's name.
     * @return The full name of the person.
     */
    public String getFullName();
    
    /**
     * Set the full name of the person.
     * @param fn the full name.
     */
    public void setFullName(String fn);
    
    /**
     * Get the person's registration number.
     * @return The registration number of the person.
     */
    public int getRegistrationNumber();
    
    /**
     * Set the registration number of the person.
     * @param rn the registration number.
     */
    public void setRegistrationNumber(int rn);
}
