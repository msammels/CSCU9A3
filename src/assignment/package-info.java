/**
 * <p><b>Assignment</b></p>
 * 
 * <p><b>CSCU9A3 Assignment: University Management System</b></p>
 * <p><span style="color: red">Due 4pm, 30th November 2020</span></p>
 * 
 * <p>
 * You will implement components of a university management system, which stores information about students, professors,
 * and modules. You will be given partly complete code, and it is your job to complete the code. Comments containing
 * “{@literal @TODO}” mark placeholders in the existing code for you to complete.
 * </p>
 * 
 * <p>
 * Test code is supplied showing you the expected output. Look at the expected output to understand what the code should
 * do. The code for the assignment is available as a .zip file on Canvas.
 * </p>
 * 
 * <p>
 * You are required to make the following enhancements to the code, each identified by numbers below. The complete
 * assignment is worth 45% of the overall grade for CSCU9A3.
 * </p>
 * 
 * <p><b>Improving {@link Student#Student Student} Class</b></p>
 * 
 * Nowadays, universities need to track more information about the students, including the programme name and whether the
 * student is an ARUAA student or not. They also want to add a way of easily controlling the number modules a student is
 * enrolled. You are given a basic Student class, but it needs the following functionality added.
 * 
 * <ol>
 *     <li>
 *     Add a {@code protected String programmeName} attribute to the {@code Student} class. Allow the value for
 *     {@code programmeName} to be read and set via the methods {@code getProgrammeName()} and {@code setProgrammeName()}.
 *     </li>
 *     <li>
 *     Create a new constructor for {@code Student} with {@code full name}, {@code registration number}, {@code email},
 *     and {@code programmeName} as parameters, and amend the other constructors to set {@code programmeName} to
 *     “Bachelor of Science”.
 *     </li>
 *     <li>
 *     Add a {@code protected boolean isARUAA} attribute to the {@code Student} class; initialise it to false in the
 *     constructors; and allow it to be read via a method {@code isARUAA()}.
 *     </li>
 *     <li>
 *     Add methods {@code isARUAAStudent} and {@code isNotARUAAStudent} to set the value of the {@code isARUAA} attribute.
 *     </li>
 *     <li>
 *     Ensure that {@code addModule()} method checks adding new modules for a student will not exceed the maximum number of
 *     modules ({@code MAX_NUM_MODULES}) before adding them, and returns an appropriate value to confirm whether the
 *     module number could be altered (true if altered, false otherwise).
 *     </li>
 * </ol>
 * 
 * <p><b>Adding a {@link MScStudent#MScStudent MScStudent} Class</b></p>
 * 
 * The universities also have post-graduate students, such as MSc. To handle these students, create a new class, called
 * {@code MScStudent}, which will inherit from the {@code Student} class and add some MScStudent-specific functionality.
 * 
 * <ol start="6">
 *     <li>
 *     Using inheritance, add a {@code MScStudent} class that has the {@code protected String} attribute
 *     {@code researchTitle} and the {@code protected Professor supervisor}.
 *     </li>
 *     <li>
 *     Add a constructor that takes the parameters full name, registration number, email, research title, and supervisor.
 *     </li>
 *     <li>
 *     Method {@code toString()} to return a string summarising the {@code MScStudent} object. This should be similar to
 *     the {@code toString()} method in {@code Student} and other existing classes (such as the {@code Professor} class).
 *     Precisely, this method should return a String containing, at least, the name of the student, the registration number,
 *     the research title, and the supervisor.
 *     </li>
 * </ol>
 * 
 * <p><b>Binary Tree Walk</b></p>
 * 
 * In our system, we have a class {@code Module} that represents the modules offered by the university. In this class,
 * we store the list of students enrolled in that module. We are using a Binary Tree, that is sorted based on the name of
 * the {@code Student} objects it contains, to do this. A {@code Node} class containing a reference to a {@code Student}
 * object has been provided for you together with a partially implemented {@code BinaryTree} class consisting of
 * {@code Node} objects. The {@code BinaryTree} class contains the implementation necessary to add Student objects to the
 * tree, method definition to walk the tree in an in-order traversal, and a method definition for a find method.
 * 
 * <br /><br />
 * 
 * The class {@code University} has a list (array list) of {@code Modules} offered by the university. This class
 * implements two important methods that handle the students enrolled in a specific module:
 * 
 * <ol>
 *     <li>
 *     {@code inWalk(val)}, which calls the in-order walk over the binary tree of students enrolled in the module in
 *     {@code index val}
 *     </li>
 *     <li>
 *     {@code find(val, name)}, which calls the method find for the module in {@code index val}
 *     </li>
 * </ol>
 * 
 * <ol start="9">
 *     <li>
 *     Your task is to complete the implementation of the protected inOrder method in {@code BinaryTree.java}
 *     </li>
 * </ol>
 * 
 * This method will return a String object containing a comma separated list of strings representing the {@code Students},
 * by calling {@code getName()} on each {@code Student} object. The initial implementation of the current {@code inWalk}
 * method will result in only the root node of the tree being returned. You must add subsequent recursive calls to ensure
 * the tree is visited in the relevant order. As a starting point and to provide some clues as to how you might write your
 * tests, the {@code go()} method of {@code UniversityTest.java} contains calls to print out the student list of a specific
 * module, print out the tree, and calls the traversal method.
 * 
 * <p><b>Searching a Binary Tree</b></p>
 * 
 * It is not much use having a more efficient structure for searching for items in a binary tree if we don’t actually have
 * a method to find the items. An outline for a {@code find()} method has been supplied for you in the class
 * {@code BinaryTree}. The {@code go()} method in {@code UniversityTest.java} makes a call to the above private find
 * method (by calling the find method in the {@code University.java} for a specific module) and provides code that will
 * determine if the {@code Student} with the name ‘John’ or ‘Jack’ has been located in a specific module.
 * 
 * <ol start="10">
 *     <li>
 *     Your task is to complete the {@code find()} method in the {@code BinaryTree.java}, such that it finds a
 *     {@code Student} with the given name or returns null if it couldn’t find it.
 *     </li>
 * </ol>
 * 
 * Remember that, to call the {@code find()} method in the {@code BinaryTree}, you need to use the public method
 * {@code find(val, name)} in the {@code University.java}, which will then call the {@code find()} method in the
 * {@code BinaryTree} for the module in index {@code val}.
 * 
 * <br /><br />
 * 
 * You should be able to reuse the logic of your tree walk to recursively implement the find method although it will
 * require a little adaptation so that it does not walk the entire tree. Although the solution only involves about
 * 8-10 lines of code, you may want to come back to this task after attempting the next problem. The challenge is
 * handling the recursive search process rather than writing many lines of code (the same is true for the tree walk).
 * 
 * <p><b>Sorting</b></p>
 * 
 * To allow the universities to properly track the current offered modules, we need to sort the original ArrayList of
 * {@code Modules} in the University class by their name and code.
 * 
 * <ol start="11">
 *     <li>
 *     Your task is to implement the {@code MergeSort} algorithm in the {@code University} class. The sorting should be
 *     based on:
 *     
 *     <ul>
 *         <li>
 *         name or code for each Module, depending on the value passed to the “attr” parameter. You can assume this is
 *         always a string with value “name” or “code”,
 *         </li>
 *         <li>
 *         ascending or descending, depending on the value passed to the “asc” parameter.
 *         </li>
 *     </ul>
 * </ol>
 *     
 * Outline code to check if the list is sorted correctly, and to print the results of attempting to do this is provided for
 * you in the {@code UniversityTest} class.
 * 
 * <p><b>Binary Search</b></p>
 * 
 * Once the array is sorted by name, it is possible to use a binary search to efficiently search it for a {@code Module}
 * with a specific name.
 * 
 * <ol start="12">
 *     <li>
 *     Implement the {@code binarySearch} method in the {@code University} class (if you are unable to implement the
 *     {@code mergeSort} algorithm above, for testing you may use a simpler sorting algorithm to sort the array list prior
 *     to searching it)
 *     </li>
 * </ol>
 * 
 * <p><b>Testing</b></p>
 * 
 * <ol start="13">
 *     <li>
 *     Write appropriate test methods for the walk and binary search in {@code UniversityTest.java} to check that
 *     they work as intended (outline test methods have been provided for you). Assuming the list of modules and students
 *     in {@code loadData()}, {@code inOrderTest()} should ensure that the walk over the students enrolled in any of these
 *     modules is conducted in the correct order. {@code BinarySearchTest()} should ensure that one module is found and one
 *     module is not found, assuming the list of modules in {@code loadData()}.
 *     </li>
 * </ol>
 * 
 * <p><b>Summary of Requirements</b></p>
 * 
 * To summarise, you are required to:
 * 
 * <ul>
 *     <li>
 *     Modify the {@code Student} class (tasks [1]-[5]) (20%)
 *     </li>
 *     <li>
 *     Create a {@code MScStudent} class (tasks [6]-[8]) (15%)
 *     </li>
 *     <li>
 *     Implement the {@code inOrder} and find methods in {@code BinaryTree} (tasks [9]-[10]) (15%)
 *     </li>
 *     <li>
 *     Implement the {@code mergeSort} method in {@code University} (task [11]) (20%)
 *     </li>
 *     <li>
 *     Implement the {@code binarySearch} method in {@code University} (task [12]) (10%)
 *     </li>
 *     <li>
 *     Write tests for the tree walk and binary search in {@code UniversityTest} (task [13]) (10%)
 *     </li>
 *     <li>
 *     Marks will also be allocated for appropriate commenting, sensible variable names, and general quality of
 *     code – efficient, concise, readable (10%)
 *     </li>
 * </ul>
 * 
 * You may also wish to create additional unit tests to the ones required above to ensure that your code is working as
 * intended although these will not be graded.
 * 
 * <p><b>Submission</b></p>
 * 
 * Please ensure that you put your student ID (but not your name) at the top of all the .java files and also ensure that
 * all your code compiles. Only a minimal attempt will be made to try to get code to compile if it appears faulty and
 * this will incur a penalty.
 * 
 * <br /><br />
 * 
 * Your code will be automatically collected from the directory {@code CSCU9A3\Assignment} in your home folder on
 * <span style="color:red">Monday the 30th of November at 4pm</span>. It is your responsibility to make sure that all the
 * required files (with their original names) are placed in the correct folder prior to the collection deadline. Please
 * check that you are able to access your home directory via the VPN, and that the {@code CSCU9A3/Assignment} folder is
 * visible to you. If you are unable to access this please let us know as soon as possible.
 * 
 * 
 * <p><b>Plagiarism</b></p>
 * 
 * Work which is submitted for assessment must be your own work. All students should note that the University has a
 * formal policy on plagiarism which can be found
 * <a href="https://www.stir.ac.uk/about/professional-services/student-academic-and-corporate-services/academic-registry/academic-policy-and-practice/quality-handbook/assessment-and-academic-misconduct/#eight">here</a>
 * 
 *  <br /><br />
 *  
 *  Plagiarism means presenting the work of others as though it were your own. The University takes a very serious view of
 *  plagiarism, and the penalties can be severe (ranging from a reduced grade in the assessment, through a fail for the
 *  module, to expulsion from the University for more serious or repeated offences). Specific guidance in relation to
 *  Computing Science assignments may be found in the Computing Science Student Handbook. We check submissions carefully
 *  for evidence of plagiarism, and pursue those cases we find.
 *  
 *  <p><b>Late submission</b></p>
 *  
 *  If you cannot meet the assignment hand-in deadline and have good cause, please see the module coordinator to explain
 *  your situation and ask for an extension. Coursework will be accepted up to seven days after the hand-in deadline
 *  (or expiry of any agreed extension) but the mark will be lowered by three marks per day or part thereof. After
 *  seven days the work will be deemed a non-submission and will receive an X.
 */
package assignment;