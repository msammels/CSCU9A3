package assignment;

/**
 * CSCU9A3 Assignment <br />
 * <code>Node.java</code>
 * 
 * <p>
 * A node in the Binary Tree. It contains a reference to a Person object associated with the node and references
 * to the left and right Node objects that are below the node.
 * </p>
 *
 * <strong>ATTENTION: you do NOT need to modify this code at all</strong>
 *
 * @author  2629330
 * @version 01.11.2020
 * @since   1.0
 */

public class Node {
    /**
     * The {@link Person} stored at this node.
     */
    private Person person = null;
    
    /**
     * The left node of this node.
     */
    private Node left = null;
    
    /**
     * The right node of this node.
     */
    private Node right = null;

    /**
     * Default constructor that initialises the node with a Page associated with this node.
     * @param a the content of the node.
     */
    public Node(Person a) {
        person = a;
    }

    /**
     * Set the left references of this node to 'n'.
     * @param n a reference to the new left node.
     */
    public void setLeft(Node n) {
        left = n;
    }

    /**
     * Set the right reference of this node to 'n'.
     * @param n a reference to the new right now.
     */
    public void setRight(Node n) {
        right = n;
    }

    // Get properties of the node
    
    /**
     * Get the person.
     * @return The person.
     */
    public Person getPerson() { return person; }
    
    /**
     * Get the person's name.
     * @return The person's full name.
     */
    public String getPersonName() { return person.getFullName(); }
    
    /**
     * Direction.
     * @return Is it left?
     */
    public boolean hasLeft() { return left != null; }
    
    /**
     * Direction.
     * @return Is it right?
     */
    public boolean hasRight() { return right != null; }
    
    /**
     * Node direction.
     * @return The left node.
     */
    public Node left() { return left; }
    
    /**
     * Node direction.
     * @return The right node.
     */
    public Node right() { return right; }

    /**
     * Comparing the nodes.
     * @param pn    the person's name.
     * @return      The comparison.
     */
    public int compareTo(Node pn) { return getPersonName().compareTo(pn.getPersonName()); }
    
    /**
     * Make the output look pretty.
     */
    @Override
    public String toString() { return getPersonName(); }
}
