package assignment;

/**
 * CSCU9A3 Assignment <br />
 * <code>Module.java</code>
 *
 * @author  2629330
 * @version 01.11.2020
 * @since   1.0
 */

public class Module {
    /**
     * Internal code of the module (e.g. 5246).
     */
    private int code;
    
    /**
     * Name of the module.
     */
    private String name;
    
    /**
     * Binary tree that stores the students enrolled in this module.
     */
    private BinaryTree btree;
    
    /**
     * Professor teaching this module.
     */
    private Professor prof;

    /**
     * Constructor.
     * @param c internal code.
     * @param n name.
     * @param p professor.
     */
    public Module(int c, String n, Professor p) {
        this.code = c;
        this.name = n;
        this.btree = new BinaryTree();  // Empty
        this.prof = p;
    }

    /**
     * Constructor.
     * @param c     internal code.
     * @param n     name.
     * @param bt    binary tree with the students enrolled in the module.
     * @param p     professor.
     */
    public Module(int c, String n, BinaryTree bt, Professor p) {
        this.code = c;
        this.name = n;
        this.btree = bt;
        this.prof = p;
    }

    /* Getters and setters! */
    
    /**
     * Get the module code.
     * @return The module code.
     */
    public int getCode() {
        return code;
    }
    
    /**
     * Set the module code.
     * @param code the module code.
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Get the module name.
     * @return The module name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Set the module name.
     * @param name the module name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the binary tree.
     * @return The binary tree.
     */
    public BinaryTree getBtree() {
        return btree;
    }
    
    /**
     * Set the binary tree.
     * @param btree the binary tree.
     */
    public void setBtree(BinaryTree btree) {
        this.btree = btree;
    }

    /**
     * Get the professor.
     * @return The professor.
     */
    public Professor getProf() {
        return prof;
    }
    
    /**
     * Set the professor.
     * @param prof the professor.
     */
    public void setProf(Professor prof) {
        this.prof = prof;
    }

    // From here, we start methods related to the binary tree that stores the list of students

    /**
     * Add a student to the binary tree.
     * @param s the student.
     */
    public void addStudent(Student s) {
        btree.addNode(s);
        s.addModule(1);
    }

    /**
     * In-order traversal method.
     * @return A string with the names of all students in the tree.
     */
    public String inWalk() {
        return btree.inOrderTraversal();
    }

    /**
     * Find a student in the binary tree using the name.
     * @param name  the name of the student you are searching for.
     * @return      A reference to the student that was found or null if no student found.
     */
    public Person find(String name) {
        return btree.find(name);
    }

    /**
     * Method for printing the tree.
     */
    public void describeStudentTree() {
        btree.printTree();
    }

    /**
     * Make the output prettier and more logically constructed.
     */
    @Override
    public String toString() {
        return "Module: " + code + " - " + name + " -- Students: " + btree.inOrderTraversal();
    }
}
