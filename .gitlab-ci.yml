#
# GitLab Pages Deployment
#
# .gitlab-ci.yml
# CSCU9A3
#
# Created by Michael Sammels.
# Copyright © 2023 Central Data Solutions. All rights reserved.
#

# Define a pipeline job named 'pages' within the 'deploy' stage.
# The 'deploy' stage is responsible for deploying the website.
pages:
  stage: deploy

  # The `script` section contains a sequence of shell commands executed during this job.
  # In this case, the commands perform the following tasks:
  # 1. Create a temporary directory named '.docs'.
  # 2. Copy all files from the current directory into the '.docs' directory.
  # 3. Rename the '.docs' directory to 'docs'.
  script:
    - mkdir .docs
    - cp -r * .docs
    - mv .docs docs

  # The 'artifacts' section specifies files preserved as artifacts after job completion.
  # Artifacts are files generated during a job's execution and retained for future reference.
  # In this case, the 'docs' directory is specified as an artifact, preserving deployed website files
  # for access later if needed.
  artifacts:
    paths:
      - docs
  
  # The 'publish' command is responsible for making the 'docs' directory accessible for GitLab Pages.
  # This command essentially publishes the website located in the 'docs' directory to GitLab Pages.
  # It's a crucial step in the deployment process, as it ensures that the website becomes publicly accessible.
  # The 'docs/' argument specifies the source directory to be published.
  publish: docs

  # The 'only' section determines conditions for running this pipeline job.
  # This pipeline runs when changes are pushed to the 'main' branch.
  # This ensures the deployment process triggers exclusively when new changes occur.
  only:
    - main

# Explanation:
#
# The above GitLab CI/CD configuration defines a pipeline job named 'pages' responsible for deploying
# the website. This job is part of the 'deploy' stage, dedicated to deployment tasks. The 'script' section
# contains a sequence of shell commands executed when the 'pages' job runs. These commands create a temporary
# directory named '.docs', copy all files from the current directory into it, and then rename it to 'docs'.
# This prepares website files for deployment. The 'artifacts' section specifies that the 'docs' directory
# should be preserved as an artifact. Artifacts are files generated during job execution and kept for future
# use. In this case, the 'docs' directory is preserved, allowing access to deployed website files. The 'only'
# section determines the pipeline job runs only when changes are pushed to the 'main' branch, ensuring the
# deployment process triggers only with new changes to the main branch. Automating deployment this way enhances
# efficiency and consistency in deploying the website whenever new changes arise.
